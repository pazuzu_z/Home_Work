package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog;

/*
1. Введите в наш цветочный киоск интерфейсы для работы продавца (может только формировать букеты и продавать их),
интерфейс владельца(устанавливает цену на цветок) и интерфейс поставщика (доставляет определенное количество цветов определенного вида по определенной стоимости).
Реализуйте класс FlowerShopExecute, который имплементирует методы всех интерфейсов.
В основном классе Main сымитируйте поставку товара, его оценку, рэндомную генерацию букетов и их распродажу до конца. Подсчитайте сумму прибыли (выторг - себестоимость)
Интерфейсы должны быть расположены в разных пакетах. Маin и FlowerShop в одном.
* */

import com.company.Executable;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.ExceptionsLog.ExceptionsLog;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Accountant.Accountant;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Owner.Owner;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Provider.Provider;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Vendor.Vendor;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.ExceptionsLog.ExceptionLogger.throwException;
import static com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.FlowerShop.getInstance;

//Class extends Executable class with it's own realization of execute() method.
public class FlowerShopWithExceptionsExecute implements Executable{

    //Method random will execute run() method  as described in the task.
    @Override
    public void execute() throws Exception {

        //Implementation of Flower shop imitation.
        //There 3 main characters. Owner, Provider, Vendor.
        //All this interfaces implements in FlowerShopExecute.

        Owner owner = getInstance();
        Provider provider = getInstance();
        Vendor vendor = getInstance();
        Accountant accountant = getInstance();

        owner.setMoney();

        //Task has 2 iterations. After first one finished all collected money reassigned to Owner from Vendor and cycle begin again.
        for (int i = 0; i < 1; i++) {

            //Run method return auto generated provider prices.
            HashMap<String, Integer> providerPrices = provider.getProviderPrices();
            //Display this prices.
            for (Map.Entry<String, Integer> flowerSet: providerPrices.entrySet()) {
                System.out.println("Provider price for " + flowerSet.getKey() + " :  " + flowerSet.getValue());
            }

            //Run method which simulate buying of flowers from vendor according to previously generated Provider prices.
            ArrayList<String> boughtFlowers = owner.buyFlowersForShop(providerPrices);
            //Display array of bought flowers.
            System.out.print("Owner bought next Flowers :  [ ");
            for (String s: boughtFlowers ) {
                System.out.print(s + ", ");
            }
            System.out.println("]");

            //Run method which generated Owners prices for the Shop.
            HashMap<Integer, Flower> shopPrices = owner.setShopPrices(providerPrices);

            //Display Shop prices.
            for (Map.Entry<Integer, Flower> flowerSet: shopPrices.entrySet()) {
                System.out.println("Shop price for " + flowerSet.getValue().getName() + " :  " + flowerSet.getValue().getCost());
            }

            //While cycle which run till all flower will be sell.
            //Simulate sell Flowers in the shop.
            while (vendor.getCountFlowersInTheShop() > 0){

                //Display which flowers left in the shop.
                System.out.print("Flowers left in the shop:  [ ");
                for (String flowersLeft : boughtFlowers ) {
                    System.out.print(flowersLeft + ", ");
                }
                System.out.println("]");

                //Run method which create Bouquet(Array list) from flowers which left in the Shop.
                ArrayList<Flower> bouquet = vendor.createBouquet(boughtFlowers);
                //Display created Bouquet.
                vendor.showBouquet(bouquet);
                //Run method which calculate how much Profit/Bouquet price Vendor took from sell bouquet. Add this calculation to total Vendors money.
                vendor.sellBouquet(bouquet);
                //Display Total profit from sell all the Bouquets in the shop.
                System.out.println("Total Profit: " + owner.getProfit());
            }
            //Display logic which show how money moves from Vendor to Owner before next iteration.
            System.out.println("Owners money before take out from Vendor:  " + owner.getMoney());
            System.out.println("Vendors money before Owner take them out:  " + vendor.getCash());
            owner.takeOutVendorsMoney();
            System.out.println("Owners money after took out from Vendor:  " + owner.getMoney());
            System.out.println("Vendors money after Owner took them out:  " + vendor.getCash() + " \n");
            System.out.println("==========================================================================================================================");
            System.out.println("==========================================================================================================================");

            //Send data to the Method which prepare Total Report.
            accountant.setAccountancy();
        }

        //Displayed Total Report.
        accountant.getAccountancy();

        //Print all logged events during Runtime.
        System.out.println("==========================================================================================================================");
        System.out.println("==========================================================================================================================");
        System.out.println("Logged Events:  ");
        ExceptionsLog exceptionsLog = ExceptionsLog.getInstance();
        exceptionsLog.getLogs();
    }
}
