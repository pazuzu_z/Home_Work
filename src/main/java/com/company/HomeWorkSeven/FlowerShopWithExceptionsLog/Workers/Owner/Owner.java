package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Owner;

import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Flower;

import java.util.ArrayList;
import java.util.HashMap;

public interface Owner {
    void setMoney();
    HashMap<Integer, Flower> setShopPrices(HashMap<String, Integer> providerPrices);
    ArrayList<String> buyFlowersForShop(HashMap<String, Integer> providerPrices);
    void takeOutVendorsMoney();
    Integer getProfit();
    Integer getMoney();
}
