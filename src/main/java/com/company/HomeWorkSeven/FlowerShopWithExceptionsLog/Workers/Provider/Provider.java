package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Provider;

import java.util.HashMap;

public interface Provider {
    HashMap<String, Integer> getProviderPrices();
}
