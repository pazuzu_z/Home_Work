package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Flowers;

import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Flower;

//Class extends Flowers class with it's own realization of bloom() method.
public class Rose extends Flower {

    public Rose(Integer price){
        setCost(price);
        setColor("RED");
        setName("ROSE");
    }

    @Override
    public void bloom() {
        System.out.println("Roses are Red and beautiful with spikes.");
    }
}
