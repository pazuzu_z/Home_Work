package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.ExceptionsLog;

import java.util.ArrayList;

//Class created for logging different events and make it simplified.
//Realization via Singleton pattern.
//Could be easily expand.
public class ExceptionsLog {
    private static ExceptionsLog instance;
    private ArrayList<String> logs = new ArrayList<>();

    private ExceptionsLog(){}

    public static ExceptionsLog getInstance(){
        if(instance == null){
            instance = new ExceptionsLog();
        }
        return instance;
    }

    //Method store information from Exceptions.
    public void setLogs(Exception exception){
        logs.add(exception.getCause().toString());
    }

    //Method store information from necessary events.
    public void setLogs(String massage){
        logs.add(massage);
    }

    public void setLogs(String massage, Exception exception){
        logs.add(massage + ":  " + exception.getCause().toString());
    }

    //Print all stored Events in the Log.
    public void getLogs() {
        for (String logString: logs) {
            System.out.println(logString);
        }
    }
}
