package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.ExceptionsLog;

import static com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.ExceptionsLog.ExceptionsLog.getInstance;

//Exception which allow to store information in Logs.
public class ExceptionLogger extends Exception {

    //Constructor made initialization plus call method which will store Exception info in the Log.
    private ExceptionLogger(Exception exception){
        ExceptionsLog log = getInstance();
        this.initCause(exception);
        log.setLogs(this);
    }

    //Constructor call method which will store Event info in the Log.
    private ExceptionLogger(String massage){
        ExceptionsLog log = getInstance();
        log.setLogs(massage);
    }

    //Constructor call method which will store Event and Exception info in the Log.
    private ExceptionLogger(String massage, Exception exception){
        ExceptionsLog log = getInstance();
        this.initCause(exception);
        log.setLogs(massage, this);
    }

    //Method initialized ExceptionLogger and process this exception.
    //Work for Exceptions
    public static void throwException(Exception exception){
        try{
            throw new ExceptionLogger(exception);
        }catch (ExceptionLogger e){
            System.out.println("Event has been logged.");
        }
    }

    //Method initialized ExceptionLogger and process this exception.
    //Work for Events.
    public static void throwException(String massage){
        try{
            throw new ExceptionLogger(massage);
        }catch (ExceptionLogger e){
            System.out.println("Event has been logged:  " + massage);
        }
    }

    //Method initialized ExceptionLogger and process this exception.
    //Work for Events and Exception.
    public static void throwException(String massage, Exception exception){
        try{
            throw new ExceptionLogger(massage, exception);
        }catch (ExceptionLogger e){
            System.out.println("Event has been logged:  " + massage);
        }
    }

}
