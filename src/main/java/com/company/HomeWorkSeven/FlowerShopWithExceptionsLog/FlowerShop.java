package com.company.HomeWorkSeven.FlowerShopWithExceptionsLog;


import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Flowers.*;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Accountant.Accountant;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Owner.Owner;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Provider.Provider;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.Workers.Vendor.Vendor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

import static com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.ExceptionsLog.ExceptionLogger.throwException;

/*
* 1. Создайте в цветочном магазине систему обработки исключений,
* которая не позволит программе остановится при возникновении исключительной ситуации.
* Симитируйте возникновение этого события.
*
2. Разработайте собственное исключение, основной задачей которого будет обработка логгирования действий.
Его мы можем выбрасывать вручную, когда нам необходимо что то заллогировать, или в случае возникновения исключительной ситуации в системе,
мы должны связать с ним exception источник и тоже его залогировать.
В него мы можем передавать код ошибки при вызове. Пока реализация логгирования будет просто выводом в консоль сообщения, найденного по коду.
*
* */

//Class implements three interfaces and all main logic happens here.
//Class implements as Singleton.
public class FlowerShop implements Owner, Vendor, Provider, Accountant {
    private static FlowerShop instance;
    private Integer countRoses = 0;
    private Integer countLily = 0;
    private Integer countCactus = 0;
    private Integer countCarnation = 0;
    private Integer countTulip = 0;
    private Integer totalCountRoses = 0;
    private Integer totalCountLily = 0;
    private Integer totalCountCactus = 0;
    private Integer totalCountCarnation = 0;
    private Integer totalCountTulip = 0;
    private boolean found;
    private Integer money;
    private Integer profit = 0;
    private Integer totalProfit = 0;
    private Integer vendorMoney = 0;
    private Integer budget = 0;
    private Integer totalBudget = 0;

    private ArrayList<String> logs  = new ArrayList<>();
    private HashMap<Integer, Flower> shopFlowersPrices = new HashMap<>();
    private HashMap<String, Integer> providerPrices;
    private ArrayList<String> boughtFlowers;



    //Singleton implementation. Cause in the future we will need that all variables of this class
    //has link to the same instance.
    private FlowerShop(){
    }

    public static FlowerShop getInstance(){
        if(instance == null){
            instance = new FlowerShop();
        }
        return instance;
    }

    //Method set money class variable. Need to put start Value of owners money.
    @Override
    public void setMoney() {
        setShopBudget();
    }

    //Method Allow to set Start Shop budget.
    private void setShopBudget(){
        this.money = 0;
        System.out.print("Enter Start Shop budget:  ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        do{
            try {
                this.money = Integer.parseInt(reader.readLine());
                if(this.money < 100 || this.money > 1000)throwException("incorrect Start Budget:  " + this.money);

            }catch (Exception exception){
                throwException("Incorrect Start Shop Budget.", exception);
            }
        }while (this.money < 100 || this.money > 1000);
    }

    //Method generates flower Prices according to Vendors Prices.
    @Override
    public HashMap<Integer, Flower> setShopPrices(HashMap<String, Integer> providerPrices){
        this.providerPrices = providerPrices;

        for (Map.Entry<String, Integer> flowersSet: providerPrices.entrySet()) {
            Integer shopFlowerPrice = flowersSet.getValue() + flowersSet.getValue() / 5;
            if(flowersSet.getKey().equals("ROSE")){shopFlowersPrices.put(1, new Rose(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("LILY")){shopFlowersPrices.put(2, new Lily(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("CACTUS")){shopFlowersPrices.put(3, new Cactus(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("CARNATION")){shopFlowersPrices.put(4, new Carnation(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("TULIP")){shopFlowersPrices.put(5, new Tulip(shopFlowerPrice) );
            }
        }
        return shopFlowersPrices;
    }


    //Method simulate buying of the flowers by Provider Prices.
    @Override
    public ArrayList<String> buyFlowersForShop(HashMap<String, Integer> providerPrices) {
        ArrayList<String> boughtFlowerList = new ArrayList<>();
        this.budget = 0;

        Object[] flowerKeys = providerPrices.keySet().toArray();

        while (true){
            Object key = flowerKeys[new Random().nextInt(flowerKeys.length)];
            if (this.money > providerPrices.get(key)){
                this.budget += providerPrices.get(key);
                this.money -= providerPrices.get(key);
                boughtFlowerList.add((String) key);

                //Call method which counts how many flower of each type Owner bought for the Shop.
                setFlowersCounts((String) key);
            }else {
                this.boughtFlowers = boughtFlowerList;
                break;
            }
        }
        return boughtFlowerList;
    }

    //Method return how many flowers left in the shop.
    @Override
    public Integer getCountFlowersInTheShop(){
        return boughtFlowers.size();
    }

    //Method take ArrayList of bought Flowers and add to Bouquet ArrayList correct flower according to the Flower Name.
    //Then this flower delete from bought Flowers.
    @Override
    public ArrayList<Flower> createBouquet(ArrayList<String> flowersInTheShop) {

        //Create List where flower for bouquet will be added.
       ArrayList<Flower> bouquet = new ArrayList<>();
       //Randomly pick count of flowers in the bouquet
        int flowersInTheBouquet = (int) (Math.random() * 3 + 4);

        //Menu which is a Hint for a user. User could pick which flower should be added to the Bouquet.
        System.out.println("Pick which Flower you want to Add to the Bouquet:");
        System.out.println("1. ROSE.");
        System.out.println("2. LILY.");
        System.out.println("3. CACTUS.");
        System.out.println("4. CARNATION.");
        System.out.println("5. TULIP.");

        //Create Map which is a reflection of a Menu.
        HashMap<Integer, String> menuMap = new HashMap<>();
        menuMap.put(1, "ROSE");
        menuMap.put(2, "LILY");
        menuMap.put(3, "CACTUS");
        menuMap.put(4, "CARNATION");
        menuMap.put(5, "TULIP");


        Integer choice;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            do{
                try {
                    choice = Integer.parseInt(reader.readLine());
                    found = false;

                    //If the shop still has some flowers.
                    if(boughtFlowers.size() != 0){
                        //Check if Flowers Shop has needed flower picked by user. If yes then this Flower will be added to the Bouquet and deleted from Flower Shop.
                        isFlowerExist(choice, bouquet, menuMap);
                        //if picked Flower absent in Flower shop then new Event will be created and Logged with special logic.
                        if(!found){
                            throwException("No Such Flower in the Shop:  " + menuMap.get(choice));
                        }

                    //If no flowers left in the Shop then new Event will be created and Logged with special logic.
                    }else {
                        throwException("No flowers in the Shop.");
                        break;
                    }

                //If any exceptions will be caught then new Event will be created and Logged with special logic.
                }catch (Exception exception) {
                    throwException(exception);
                }

            }while (bouquet.size() < flowersInTheBouquet);

        //If any exceptions will be caught then new Event will be created and Logged with special logic.
        }catch (Exception exception) {
            throwException(exception);
        }

        return bouquet;
    }

    //Add flower to the Bouquet according to user choice.
    private void addFlowerToBouquet(Integer choice, ArrayList<Flower> bouquet){
        for (Map.Entry<Integer, Flower> flower: shopFlowersPrices.entrySet() ) {
            if(choice.equals(flower.getKey())){
                bouquet.add(flower.getValue());
                //Stop cycle if find correct expression for IF.
                break;
            }
        }
    }

    //Check is Flower exists in the Shop. If yes then called method which add it to the Bouquet and remove from the Shop.
    private void isFlowerExist(Integer choice, ArrayList<Flower> bouquet, HashMap<Integer, String> menuMap){
        for (String shopFlower: boughtFlowers) {
            if(menuMap.get(choice).equals(shopFlower)){
                found = true;
                System.out.println("Flower " + shopFlower + " added to the Bouquet.");
                //Remove flower from the shop and add it to bouquet.
                boughtFlowers.remove(shopFlower);
                addFlowerToBouquet(choice, bouquet);
                //Stop cycle if find correct expression for IF.
                break;
            }
        }
    }

    //Simulate sell bouquet.
    @Override
    public void sellBouquet(ArrayList<Flower> bouquet) {
        Integer bouquetPrice = 0;

        //Calculate how much bouquet will be cost and how much profit Vendor will take from sell of the concrete bouquet.
        //Save this in the class variables.
        for (Flower flower: bouquet) {
            Integer sellFlowerProfit = sellFlowerProfit(flower);
            bouquetPrice += flower.getCost();
            System.out.println("Profit from " + flower.getName() + " selling " + sellFlowerProfit);
            this.profit += sellFlowerProfit;
        }
        System.out.println("Bouquet Price:  " + bouquetPrice);
        this.vendorMoney += bouquetPrice;
    }

    //Method which moves money from Vendor to the Owner.
    @Override
    public void takeOutVendorsMoney(){
        this.money += this.vendorMoney;
        this.vendorMoney = 0;
    }

    //Method return earned profit.
    @Override
    public Integer getProfit(){
        return this.profit;
    }

    //Method displayed created Bouquet.
    @Override
    public void showBouquet (ArrayList<Flower> flowerArrayList){
        System.out.println("\nNew Bouquet created:");
        for (Flower flower: flowerArrayList) {
            System.out.println(flower);
        }
        System.out.println();
    }

    //Method get Owners money.
    @Override
    public Integer getMoney(){
        return this.money;
    }

    //Method get Owners money.
    public Integer getCash(){
        return this.vendorMoney;
    }

    //Method returns Providers prices.
    public HashMap<String, Integer> getProviderPrices(){

        HashMap<String, Integer> providerPrices = new HashMap<>();
        providerPrices.put("ROSE", (int) (Math.random() * 20 + 80));
        providerPrices.put("LILY", (int) (Math.random() * 10 + 40));
        providerPrices.put("CACTUS", (int) (Math.random() * 100 + 50));
        providerPrices.put("CARNATION", (int) (Math.random() * 20 + 20));
        providerPrices.put("TULIP", (int) (Math.random() * 5 + 15));

        return providerPrices;
    }

    //Method calculate type of the flowers which sent.
    private void setFlowersCounts(String flower){
        if(flower.equals("ROSE")){
            //this.monthCountRoses ++;
            this.countRoses ++;
            this.totalCountRoses ++;}
        else if(flower.equals("LILY")){
            this.countLily ++;
            this.totalCountLily ++;}
        else if(flower.equals("CACTUS")){
            this.countCactus ++;
            this.totalCountCactus ++;}
        else if(flower.equals("CARNATION")){
            this.countCarnation ++;
            this.totalCountCarnation ++;}
        else if(flower.equals("TULIP")){
            this.countTulip ++;
            this.totalCountTulip ++;}
    }

    //Method calculate profit for concrete sell Flower.
    //Helps to calculate general profit for the bouquet.
    private Integer sellFlowerProfit(Flower flower){

        for (Map.Entry<String, Integer> providerPricesSet: providerPrices.entrySet()) {
            if(providerPricesSet.getKey().equals(flower.getName())){
                return flower.getCost() - providerPricesSet.getValue();
            }
        }
        return null;
    }

    //Aggregate data for each iterations and send it to the method which will create Report.
    @Override
    public void setAccountancy(){
        String accauntence;
        accauntence =  "   " + getCountRoses() +
                setSpaces(getCountLily()) + getCountLily() +
                setSpaces(getCountCactus()) + getCountCactus() +
                setSpaces(getCountCarnation()) + getCountCarnation() +
                setSpaces(getCountTulip()) + getCountTulip() +
                setSpaces((getCountRoses() + getCountLily() + getCountCactus() + getCountCarnation() + getCountTulip())) + (getCountRoses() + getCountLily() + getCountCactus() + getCountCarnation() + getCountTulip()) +
                setSpaces(this.budget) + this.budget +
                setSpaces(getProfit()) + getProfit() +"   |";

        saveLogs(accauntence);
        this.totalProfit += this.profit;
        this.totalBudget += this.budget;
        nullCounts();
    }

    private String setSpaces(Integer value){
        if(value < 10)return "    |    ";
        if(value > 9 && value < 100)return "   |    ";
        if(value > 99 && value < 1000)return "  |    ";
        if(value > 999 )return " |    ";
        return " |    ";
    }

    private void saveLogs(String log){
        logs.add(log);
    }

    //Print Report. Take Base report from getTotal() method.
    @Override
    public void getAccountancy(){
        printHead();

        ArrayList<String> monthList = new ArrayList<>();
        Collections.addAll(monthList, "JAN", "FED", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
        for (int i = 0; i < logs.size(); i++) {
            System.out.print("|   " + monthList.get(i) + "   |");
            System.out.println(logs.get(i));
        }
        System.out.println(getTotal());

    }

    //Create the base of Report.
    private String getTotal(){
        return  "|========================================================================================|\n" +
                "|  TOTAL  |  " + this.totalCountRoses +
                        "   |  " + this.totalCountLily +
                        "    |  " + this.totalCountCactus +
                        "    |  " + this.totalCountCarnation +
                        "   |  " + this.totalCountTulip +
                        "    ||   " + (this.totalCountRoses + this.totalCountLily + this.totalCountCactus + this.totalCountCarnation + this.totalCountTulip) +
                        " |  " + this.totalBudget +
                        "  |    " + this.totalProfit +"   |";

    }

    //Create head of the report.
    private void printHead(){
        System.out.println(
                "\n-----------------------------------------------------------------------------------------\n" +
                "|  MONTH  |  ROSE  |  LILY  |  CACTUS  |CARNATION| TULIP || TOTAL |  BUDGET  |  PROFIT  |\n" +
                "|---------------------------------------------------------------------------------------|" );
    }

    private Integer getCountRoses() {
        return this.countRoses;
    }

    private Integer getCountLily() {
        return this.countLily;
    }

    private Integer getCountCactus() {
        return this.countCactus;
    }

    private Integer getCountCarnation() {
        return this.countCarnation;
    }

    private Integer getCountTulip() {
        return this.countTulip;
    }

    //Method which set counts to NULL. Run after each iteration.
    private void nullCounts(){
        this.countRoses = 0;
        this.countLily = 0;
        this.countCactus = 0;
        this.countCarnation = 0;
        this.countTulip = 0;
        this.profit = 0;
    }
}
