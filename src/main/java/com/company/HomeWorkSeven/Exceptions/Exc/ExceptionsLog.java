package com.company.HomeWorkSeven.Exceptions.Exc;

import java.util.ArrayList;

public class ExceptionsLog {
    private static ExceptionsLog instance;
    private ArrayList<String> logs = new ArrayList<>();

    private ExceptionsLog(){}

    public static ExceptionsLog getInstance(){
        if(instance == null){
            instance = new ExceptionsLog();
        }
        return instance;
    }

    public void setLogs(Exception exception){
        logs.add(exception.getCause().toString());
    }

    public void getLogs() {
        for (String logString: logs) {
            System.out.println(logString);
        }
    }
}
