package com.company.HomeWorkSeven.Exceptions.Exc;

import static com.company.HomeWorkSeven.Exceptions.Exc.ExceptionsLog.getInstance;

public class MyExc extends Exception {

    private MyExc(Exception exception){
        ExceptionsLog log = getInstance();
        this.initCause(exception);
        log.setLogs(this);
    }

    public static void throwException(Exception exception){
        try{
            throw new MyExc(exception);
        }catch (MyExc e){
            System.out.println("Event has been logged.");
        }
    }

}
