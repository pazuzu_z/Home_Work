package com.company.HomeWorkSeven.Exceptions.Exc;

import com.company.Executable;

import java.io.BufferedReader;
import java.io.InputStreamReader;


import static com.company.HomeWorkSeven.Exceptions.Exc.ExceptionsLog.getInstance;
import static com.company.HomeWorkSeven.Exceptions.Exc.MyExc.throwException;


public class ExcThrow implements Executable{
    @Override
    public void execute() {
        excThrow();
    }

    public void excThrow(){

            int count = 0;

            do{
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter value: ");
                try {
                    Integer.parseInt(reader.readLine());
                }catch (Exception exception){
                    throwException(exception);
                }
                count ++;

            }while (count < 5);
        ExceptionsLog exceptionsLog = getInstance();
        exceptionsLog.getLogs();

    }
}
