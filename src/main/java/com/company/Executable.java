package com.company;

public interface Executable {
    void execute() throws Exception;
}
