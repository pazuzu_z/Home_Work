package com.company.HomeWorkThree;

interface ISkeleton {

    void setArray(int size);
    void printAllArrayElements();
    void put();
    Integer pop();
    Integer check();
}
