package com.company.HomeWorkThree;

import com.company.Executable;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Execute implements Executable{

    public void execute() throws Exception {

    }

    public static void executable(int flag)throws Exception{

        //Create variable of Interface depends on received flag.
        ISkeleton createStackOrQueue = instanceCreator(flag);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer choice;
        Integer value;
        Integer size;

        //Set the Array size
        System.out.print("Enter Array Size: ");
        size = Integer.parseInt(reader.readLine());
        createStackOrQueue.setArray(size);
        System.out.println();

        //Described Interface for communication with Array.
        System.out.println("Pick Actions: ");
        System.out.println("1. Add Element to Array.");
        System.out.println("2. Take out Element from Array.");
        System.out.println("3. Check Next element.");
        System.out.println("4. Show all elements in the Array.");
        System.out.println("Any other. Exit.");

        //Implements Interface for working with QueueWithPriority.
        try {
            do {
                choice = Integer.parseInt(reader.readLine());
                switch (choice){
                    case 1:
                        //Call method which will Added new Values to the Arrays with Values and Priorities
                        //If Arrays already overloaded then Arrays will be resized.
                        createStackOrQueue.put();
                        System.out.println("New value added to Array.");
                        break;

                    case 2:
                        //Call method which take out values from Arrays with Values and Priorities.
                        //Return Only Value. NOT priority.
                        //If called for the Queue then both Arrays will be moved to the right at 1 position.
                        //After Value will be pop out.
                        value = createStackOrQueue.pop();
                        if(value != null) System.out.println("Value " + value + " took out from Array");
                        else System.out.println("Array is empty.");
                        break;

                    case 3:
                        //Return Next value from Queue and Stack.
                        value = createStackOrQueue.check();
                        if(value != null) System.out.println("Next value in Array is " + value);
                        else System.out.println("Array is empty.");
                        break;

                    case 4:
                        //Print Arrays with Values and Priorities.
                        createStackOrQueue.printAllArrayElements();
                        break;
                }

            }while (choice > 0 && choice < 5);


        }catch (NumberFormatException e){
            System.out.println("Exit");
        }catch (NullPointerException e){
            System.out.println("Exit");
        }

        //Close the Stream.
        reader.close();
    }

    //Method return one of required instance depends on received flag
    private static ISkeleton instanceCreator(int flag){
        if (flag == 7)return new StackWithPriority();
        else if(flag == 8) return new QueueWithPriority();
        else return null;
    }
}
