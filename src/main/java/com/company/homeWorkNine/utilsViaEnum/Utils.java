package com.company.homeWorkNine.utilsViaEnum;

public enum Utils {
    ;

    Utils(){
        throw new IllegalStateException("Это не перечисление");
    }

    public static void showMassage(){
        System.out.println("Some message.");
    }

    public static void showOtherData(){
        System.out.println("Some other data.");
    }
}
