package com.company.homeWorkNine.factoryViaEnum;

import com.company.Executable;

import static com.company.homeWorkNine.factoryViaEnum.CreateWatch.createWatch;

public class FactoryRunner implements Executable {

    public void execute() {
        createWatch();
    }
}
