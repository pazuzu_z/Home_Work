package com.company.homeWorkNine.factoryViaEnum;


import com.company.homeWorkNine.factoryViaEnum.watch.AnalogWatch;
import com.company.homeWorkNine.factoryViaEnum.watch.ElectronicWatch;
import com.company.homeWorkNine.factoryViaEnum.watch.Watch;

public enum WatchFactory {
    WATCH_MAKER;

    public Watch createWatch(String watchType){

        if (watchType.equals("Electronic"))return new ElectronicWatch();
        else if (watchType.equals("Analog"))return new AnalogWatch();
        else throw new RuntimeException("Wrong watch type.");
    }
}
