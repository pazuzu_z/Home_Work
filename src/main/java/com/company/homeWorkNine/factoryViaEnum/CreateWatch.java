package com.company.homeWorkNine.factoryViaEnum;

import com.company.homeWorkNine.factoryViaEnum.watch.Watch;

public class CreateWatch {
    public static void createWatch(){
        Watch watch_1 = WatchFactory.WATCH_MAKER.createWatch("Electronic");
        Watch watch_2 = WatchFactory.WATCH_MAKER.createWatch("Analog");

        watch_1.showTime();
        watch_2.showTime();
    }
}
