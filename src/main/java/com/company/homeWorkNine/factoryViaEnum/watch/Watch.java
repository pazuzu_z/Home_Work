package com.company.homeWorkNine.factoryViaEnum.watch;

public interface Watch {
    void showTime();
}
