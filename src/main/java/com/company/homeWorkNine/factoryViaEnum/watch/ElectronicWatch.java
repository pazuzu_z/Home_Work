package com.company.homeWorkNine.factoryViaEnum.watch;

public class ElectronicWatch implements Watch {

    public void showTime() {
        System.out.println("Show time via digits");
    }
}
