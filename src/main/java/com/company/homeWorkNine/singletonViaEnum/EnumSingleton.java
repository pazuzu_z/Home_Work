package com.company.homeWorkNine.singletonViaEnum;

import com.company.Executable;

import static com.company.homeWorkNine.singletonViaEnum.RequestProperties.requestProperties;

public class EnumSingleton implements Executable {
    @Override
    public void execute() {
        requestProperties();
    }
}
