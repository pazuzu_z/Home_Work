package com.company.homeWorkNine.singletonViaEnum;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public enum PropertiesLoader {
    INSTANCE;
    private Properties properties;

    PropertiesLoader(){
        propertiesLoader();
    }

    private void propertiesLoader(){
        properties = new Properties();
        try(FileInputStream fileInputStream = new FileInputStream(Paths.PROPERTIES_PATH)){
            properties.load(fileInputStream);
        }catch (IOException e){
            throw new RuntimeException("Can't get properties:  ", e);
        }
    }

    public String getFirstProperty(){ return properties.getProperty("First");}
    public String getSecondProperty(){ return properties.getProperty("Second");}
}
