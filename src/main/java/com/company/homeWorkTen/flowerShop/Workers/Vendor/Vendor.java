package com.company.homeWorkTen.flowerShop.Workers.Vendor;

import com.company.homeWorkTen.flowerShop.Flower;

import java.util.ArrayList;

public interface Vendor {
    ArrayList<Flower> createBouquet(ArrayList<String> flowersInTheShop);
    void sellBouquet(ArrayList<Flower> bouquet);
    void showBouquet(ArrayList<Flower> flowerArrayList);
    Integer getCountFlowersInTheShop();
    Integer getCash();
}
