package com.company.homeWorkTen.flowerShop.Workers.Provider;

import java.util.HashMap;

public interface Provider {
    HashMap<String, Integer> getProviderPrices();
}
