package com.company.homeWorkTen.flowerShop.Workers.Owner;

import com.company.homeWorkTen.flowerShop.Flower;

import java.util.ArrayList;
import java.util.HashMap;

public interface Owner {
    void setMoney(Integer startMoney);
    HashMap<Integer, Flower> setShopPrices(HashMap<String, Integer> providerPrices);
    ArrayList<String> buyFlowersForShop(HashMap<String, Integer> providerPrices);
    //Integer getCountRoses();
    //Integer getCountLily();
    void takeOutVendorsMoney();
    Integer getProfit();
    Integer getMoney();
}
