package com.company.homeWorkTen.flowerShop;


import com.company.homeWorkTen.flowerShop.Flowers.*;
import com.company.homeWorkTen.flowerShop.Workers.Accountant.Accountant;
import com.company.homeWorkTen.flowerShop.Workers.Owner.Owner;
import com.company.homeWorkTen.flowerShop.Workers.Provider.Provider;
import com.company.homeWorkTen.flowerShop.Workers.Vendor.Vendor;

import java.util.*;

//Class implements three interfaces and all main logic happens here.
//Class implements as Singleton.
public class FlowerShop implements Owner, Vendor, Provider, Accountant {
    private static FlowerShop instance;
    private Integer countRoses = 0;
    private Integer countLily = 0;
    private Integer countCactus = 0;
    private Integer countCarnation = 0;
    private Integer countTulip = 0;
    private Integer totalCountRoses = 0;
    private Integer totalCountLily = 0;
    private Integer totalCountCactus = 0;
    private Integer totalCountCarnation = 0;
    private Integer totalCountTulip = 0;
    private Integer money;
    private Integer profit = 0;
    private Integer totalProfit = 0;
    private Integer vendorMoney = 0;
    private Integer budget = 0;
    private Integer totalBudget = 0;

    private ArrayList<String> logs  = new ArrayList<>();
    private HashMap<Integer, Flower> shopFlowersPrices = new HashMap<>();
    private HashMap<String, Integer> providerPrices;
    private ArrayList<String> boughtFlowers;



    //Singleton implementation. Cause in the future we will need that all variables of this class
    //has link to the same instance.
    private FlowerShop(){
    }

    public static FlowerShop getInstance(){
        if(instance == null){
            instance = new FlowerShop();
        }
        return instance;
    }

    //Method set money class variable. Need to put start Value of owners money.
    @Override
    public void setMoney(Integer money) {
        this.money = money;
    }

    //Method generates flower Prices according to Vendors Prices.
    @Override
    public HashMap<Integer, Flower> setShopPrices(HashMap<String, Integer> providerPrices){
        this.providerPrices = providerPrices;

        for (Map.Entry<String, Integer> flowersSet: providerPrices.entrySet()) {
            Integer shopFlowerPrice = flowersSet.getValue() + flowersSet.getValue() / 5;
            if(flowersSet.getKey().equals("ROSE")){shopFlowersPrices.put(1, new Rose(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("LILY")){shopFlowersPrices.put(2, new Lily(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("CACTUS")){shopFlowersPrices.put(3, new Cactus(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("CARNATION")){shopFlowersPrices.put(4, new Carnation(shopFlowerPrice) );
            }else if(flowersSet.getKey().equals("TULIP")){shopFlowersPrices.put(5, new Tulip(shopFlowerPrice) );
            }
        }
        return shopFlowersPrices;
    }


    //Method simulate buying of the flowers by Provider Prices.
    @Override
    public ArrayList<String> buyFlowersForShop(HashMap<String, Integer> providerPrices) {
        ArrayList<String> boughtFlowerList = new ArrayList<>();
        this.budget = 0;

        Object[] flowerKeys = providerPrices.keySet().toArray();

        while (true){
            Object key = flowerKeys[new Random().nextInt(flowerKeys.length)];
            if (this.money > providerPrices.get(key)){
                this.budget += providerPrices.get(key);
                this.money -= providerPrices.get(key);
                boughtFlowerList.add((String) key);

                //Call method which counts how many flower of each type Owner bought for the Shop.
                setFlowersCounts((String) key);
            }else {
                this.boughtFlowers = boughtFlowerList;
                break;
            }
        }
        return boughtFlowerList;
    }

    //Method return how many flowers left in the shop.
    @Override
    public Integer getCountFlowersInTheShop(){
        return boughtFlowers.size();
    }

    //Method simulate Bouquet creation. This is simple implementation.
    //Method take ArrayList of bought Flowers and add to Bouquet ArrayList correct flower according to the Flower Name.
    //Then this flower delete from bought Flowers.
    //NOTE:
    //This is the temporary implementation.
    //There could be one of next two implementations:
    //1. Allow user to pick which flower thy want to add to the bouquet.
    //2. Make a random choose.(There could be a difficulties how implements when one or same type of Flowers finished.)
    @Override
    public ArrayList<Flower> createBouquet(ArrayList<String> flowersInTheShop) {

        //Create List where flower for bouquet will be added.
       ArrayList<Flower> bouquet = new ArrayList<>();
       //Randomly pick count of flowers in the bouquet
        int flowersInTheBouquet = (int) (Math.random() * 3 + 4);

        //This part allows to create bouquet if in The shop flowers more then should be in the bouquet.
        if(flowersInTheShop.size() > flowersInTheBouquet){

            for (int i = 0; i < flowersInTheBouquet; i++) {
                for (Map.Entry<Integer, Flower> flower: shopFlowersPrices.entrySet() ) {
                    if(flowersInTheShop.get(0).equals(flower.getValue().getName())){
                        bouquet.add(flower.getValue());
                        flowersInTheShop.remove(flowersInTheShop.get(0));
                        break;
                    }
                }
            }
        //This part allows to create bouquet if in The shop flowers less then should be in the bouquet.
        }else if (flowersInTheShop.size() < flowersInTheBouquet && flowersInTheShop.size() > 0){
            for (int i = 0; i < flowersInTheShop.size(); i++) {

                for (Map.Entry<Integer, Flower> flower: shopFlowersPrices.entrySet() ) {
                    if(flowersInTheShop.get(0).equals(flower.getValue().getName())){
                        bouquet.add(flower.getValue());
                        flowersInTheShop.remove(flowersInTheShop.get(0));
                        break;
                    }
                }
            }
        }
        return bouquet;
    }

    //Simulate sell bouquet.
    @Override
    public void sellBouquet(ArrayList<Flower> bouquet) {
        Integer bouquetPrice = 0;

        //Calculate how much bouquet will be cost and how much profit Vendor will take from sell of the concrete bouquet.
        //Save this in the class variables.
        for (Flower flower: bouquet) {
            Integer sellFlowerProfit = sellFlowerProfit(flower);
            bouquetPrice += flower.getCost();
            System.out.println("Profit from " + flower.getName() + " selling " + sellFlowerProfit);
            this.profit += sellFlowerProfit;
        }
        System.out.println("Bouquet Price:  " + bouquetPrice);
        this.vendorMoney += bouquetPrice;
    }

    //Method which moves money from Vendor to the Owner.
    @Override
    public void takeOutVendorsMoney(){
        this.money += this.vendorMoney;
        this.vendorMoney = 0;
    }

    //Method return earned profit.
    @Override
    public Integer getProfit(){
        return this.profit;
    }

    //Method displayed created Bouquet.
    @Override
    public void showBouquet (ArrayList<Flower> flowerArrayList){
        System.out.println("\nNew Bouquet created:");
        for (Flower flower: flowerArrayList) {
            System.out.println(flower);
        }
        System.out.println();
    }

    //Method get Owners money.
    @Override
    public Integer getMoney(){
        return this.money;
    }

    //Method get Owners money.
    public Integer getCash(){
        return this.vendorMoney;
    }

    //Method returns Providers prices.
    public HashMap<String, Integer> getProviderPrices(){

        HashMap<String, Integer> providerPrices = new HashMap<>();
        providerPrices.put("ROSE", (int) (Math.random() * 20 + 80));
        providerPrices.put("LILY", (int) (Math.random() * 10 + 40));
        providerPrices.put("CACTUS", (int) (Math.random() * 100 + 50));
        providerPrices.put("CARNATION", (int) (Math.random() * 20 + 20));
        providerPrices.put("TULIP", (int) (Math.random() * 5 + 15));

        return providerPrices;
    }

    //Method calculate type of the flowers which sent.
    private void setFlowersCounts(String flower){
        if(flower.equals("ROSE")){this.countRoses ++;
            this.totalCountRoses ++;}
        else if(flower.equals("LILY")){this.countLily ++;
            this.totalCountLily ++;}
        else if(flower.equals("CACTUS")){this.countCactus ++;
            this.totalCountCactus ++;}
        else if(flower.equals("CARNATION")){this.countCarnation ++;
            this.totalCountCarnation ++;}
        else if(flower.equals("TULIP")){this.countTulip ++;
            this.totalCountTulip ++;}
    }

    //Method calculate profit for concrete sell Flower.
    //Helps to calculate general profit for the bouquet.
    private Integer sellFlowerProfit(Flower flower){

        for (Map.Entry<String, Integer> providerPricesSet: providerPrices.entrySet()) {
            if(providerPricesSet.getKey().equals(flower.getName())){
                return flower.getCost() - providerPricesSet.getValue();
            }
        }
        return null;
    }

    //Aggregate data for each iterations and send it to the method which will create Report.
    @Override
    public void setAccountancy(){
        String accauntence;
        accauntence =  "   " + getCountRoses() +
                setSpaces(getCountLily()) + getCountLily() +
                setSpaces(getCountCactus()) + getCountCactus() +
                setSpaces(getCountCarnation()) + getCountCarnation() +
                setSpaces(getCountTulip()) + getCountTulip() +
                setSpaces((getCountRoses() + getCountLily() + getCountCactus() + getCountCarnation() + getCountTulip())) + (getCountRoses() + getCountLily() + getCountCactus() + getCountCarnation() + getCountTulip()) +
                setSpaces(this.budget) + this.budget +
                setSpaces(getProfit()) + getProfit() +"   |";

        saveLogs(accauntence);
        this.totalProfit += this.profit;
        this.totalBudget += this.budget;
        nullCounts();
    }

    private String setSpaces(Integer value){
        if(value < 10)return "    |    ";
        if(value > 9 && value < 100)return "   |    ";
        if(value > 99 && value < 1000)return "  |    ";
        if(value > 999 )return " |    ";
        return " |    ";
    }

    private void saveLogs(String log){
        logs.add(log);
    }

    //Print Report. Take Base report from getTotal() method.
    @Override
    public void getAccountancy(){
        printHead();

        ArrayList<String> monthList = new ArrayList<>();
        Collections.addAll(monthList, "JAN", "FED", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
        for (int i = 0; i < logs.size(); i++) {
            System.out.print("|   " + monthList.get(i) + "   |");
            System.out.println(logs.get(i));
        }
        System.out.println(getTotal());

    }

    //Create the base of Report.
    private String getTotal(){
        return  "|========================================================================================|\n" +
                "|  TOTAL  |  " + this.totalCountRoses +
                        "   |  " + this.totalCountLily +
                        "    |  " + this.totalCountCactus +
                        "    |  " + this.totalCountCarnation +
                        "   |  " + this.totalCountTulip +
                        "    ||   " + (this.totalCountRoses + this.totalCountLily + this.totalCountCactus + this.totalCountCarnation + this.totalCountTulip) +
                        " |  " + this.totalBudget +
                        "  |    " + this.totalProfit +"   |";

    }

    //Create head of the report.
    private void printHead(){
        System.out.println(
                "\n-----------------------------------------------------------------------------------------\n" +
                "|  MONTH  |  ROSE  |  LILY  |  CACTUS  |CARNATION| TULIP || TOTAL |  BUDGET  |  PROFIT  |\n" +
                "|---------------------------------------------------------------------------------------|" );
    }

    private Integer getCountRoses() {
        return this.countRoses;
    }

    private Integer getCountLily() {
        return this.countLily;
    }

    private Integer getCountCactus() {
        return this.countCactus;
    }

    private Integer getCountCarnation() {
        return this.countCarnation;
    }

    private Integer getCountTulip() {
        return this.countTulip;
    }

    //Method which set counts to NULL. Run after each iteration.
    private void nullCounts(){
        this.countRoses = 0;
        this.countLily = 0;
        this.countCactus = 0;
        this.countCarnation = 0;
        this.countTulip = 0;
        this.profit = 0;
    }
}
