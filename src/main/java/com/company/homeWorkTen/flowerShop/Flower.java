package com.company.homeWorkTen.flowerShop;

//Abstract class which describe general characteristics of all Flowers.
//All Flowers will be extends it.
//It has setters for all parameters and one getter for parameter which should be called outside.
//Also it override toString method.
public abstract class Flower {
    private int cost;
    private String color;
    private String name;

    //Abstract method general to all Flowers but should have different realization.
    public abstract void bloom();

    public int getCost() {
        return this.cost;
    }
    public String getName(){return this.name;}

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "This flower Name: " + this.name + " Color: " + this.color + " Price: " + this.cost;
    }
}
