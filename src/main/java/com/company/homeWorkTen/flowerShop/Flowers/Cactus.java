package com.company.homeWorkTen.flowerShop.Flowers;

import com.company.homeWorkTen.flowerShop.Flower;

//Class extends Flowers class with it's own realization of bloom() method.
public class Cactus extends Flower {

    public Cactus(Integer price){
        setCost(price);
        setColor("GREEN");
        setName("CACTUS");
    }

    @Override
    public void bloom() {
        System.out.println("Cactus are Green and ugly with spikes");
    }

}
