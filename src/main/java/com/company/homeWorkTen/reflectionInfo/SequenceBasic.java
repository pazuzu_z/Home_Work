package com.company.homeWorkTen.reflectionInfo;

import com.company.homeWorkTen.reflectionInfo.interfaces.IAdditionInfo;
import com.company.homeWorkTen.reflectionInfo.interfaces.IGeneral;

public abstract class SequenceBasic implements IAdditionInfo, IGeneral {
    public Integer array[][];

    public void setArray(int size){
        int priority = 2;
        this.array = new Integer[priority][size];
    }

    //Method allows to put new generated value to the Array.
    public void put(){

        //This part recreate Array when it overloaded.
        if(array[0][0] != null){
            array = resizeArrayUp();
        }

        //Add element into Array
        Integer value = (int) (Math.random() * 10);
        Integer priority = (int) (Math.random() * 10);

        for (int i = array[0].length - 1; i > - 1; i --) {
            if(array[0][i] == null){
                array[0][i] = value;
                array[1][i] = priority;

                break;
            }
        }
        prioritizeSorting();

    }

    //Method which run after new Element added to Arrays with Values and Priorities
    //It sorts both Arrays according to Priorities. 10 highest Priority 0 the lowest.
    //On the Top should be Values with Highest priority.
    public void prioritizeSorting(){

        for (int i = 0; i < array[1].length; i++) {
            for (int j = array[1].length - 2; j >= 0; j --) {
                if(array[1][j] != null && array[1][j] > array[1][j + 1]){

                    //Sort values according to priorities
                    int tempValue = array[0][j];
                    array[0][j] = array[0][j + 1];
                    array[0][j + 1] = tempValue;

                    //Sort Priorities from highest to lowest
                    int tempPriority = array[1][j];
                    array[1][j] = array[1][j + 1];
                    array[1][j + 1] = tempPriority;
                }
            }
        }
    }

    //Method allows to take Integer value from Array.
    //When Integer take out from Array it will be deleted.
    //And All Array will be moved to the Right in to one position.
    public abstract Integer pop();

    //Method show the value which Element will be the Next.
    public abstract Integer check();

    //Method allows to see visual Array condition and what values it contains
    public void printAllElements(){
        String s = "";
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                s = s + array[i][j] + " ";
            }
             if(i == 0) {
                 System.out.println("Array Values:   [ " + s + "]");
                 s = "";
             }else {
                 System.out.println("Array Priority: [ " + s + "]");
             }
        }
    }

    //This method resize array when it overloaded.
    //It create new Array with bigger size and re-assign and return it instead of overloaded
    private Integer[][] resizeArrayUp(){

        Integer[][] arrayTemp;

        //If array has small size then it will be resized to Array + 5
        //If array bigger then 1 then it will be resized to Array + Array / 2
        if(array[0].length == 0 || array[0].length == 1){
            arrayTemp = new Integer[2][array[0].length + 5];
        }else arrayTemp = new Integer[2][array[0].length + array[0].length / 2];

        //Copy all values from Income old array to New array.
        for (int i = 0; i < 2; i ++) {
            for (int j = array[i].length - 1; j >= 0 ; j --) {
                arrayTemp[i][arrayTemp[i].length - j - 1] = array[i][array[i].length - j - 1];
            }
        }
        System.out.println("Array overloaded and resized.");

        //Return new Array with all values from Old
        return arrayTemp;
    }

    //Method allow to resize Array.
    //It could be called if size of array is big but array is almost empty.
    public Integer[][] resizeArrayDown(){

        Integer[][] arrayTemp = new Integer[2][array[0].length / 2 + 1];

        for (int i = 0; i < 2; i ++) {
            for (int j = arrayTemp[i].length - 1; j >= 0 ; j --) {
                arrayTemp[i][arrayTemp[i].length - j - 1] = array[i][array[i].length - j - 1];
            }
        }
        System.out.println("Array to empty and resized.");

        return arrayTemp;
    }

}
