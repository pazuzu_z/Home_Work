package com.company.homeWorkTen.reflectionInfo.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TestAnno {
    String name();
    int value();
}
