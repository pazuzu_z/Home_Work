package com.company.homeWorkTen.reflectionInfo.queue;


import com.company.homeWorkTen.reflectionInfo.SequenceBasic;
import com.company.homeWorkTen.reflectionInfo.interfaces.IAdditionInfo;

public class QueueWithPriority extends SequenceBasic implements IAdditionInfo {

    //Method allows to take Integer value from QueueWithPriority.
    //When Integer take out from QueueWithPriority it will be deleted.
    //And All QueueWithPriority will be moved to the Right in to one position.
    public Integer pop(){
        Integer value = null;

        if(array[0][array[0].length - 1] != null){
            value = array[0][array[0].length - 1];
            shift();
        }

        //If Array is too big but empty it will be resized into twice smaller
        if(array[0].length > 100 && array[0][array[0].length / 2] == null){
            array = resizeArrayDown();
        }

        return value;
    }

    //Method show the value which Element will be the Next.
    public Integer check(){
        Integer value = null;
        if(array[0][array[0].length - 1] != null) value = array[0][array[0].length - 1];
        return value;
    }

    //Method which move QueueWithPriority when element pop.
    private void shift(){
        for (int i = 0; i < array.length; i++) {
            for (int j = array[i].length - 1; j >= 0 ; j --) {
                if(j == 0){
                    array[i][j] = null;
                }else array[i][j] = array[i][j - 1];
            }
        }
    }
}
