package com.company.homeWorkTen.reflectionInfo.queue;

import com.company.Executable;
import com.company.homeWorkTen.reflectionInfo.CreateInstance;

public class StartQueue implements Executable {
    @Override
    public void execute() throws Exception{
        new CreateInstance().run("Queue");
    }
}
