package com.company.homeWorkTen.reflectionInfo.stack;

import com.company.Executable;
import com.company.homeWorkTen.reflectionInfo.CreateInstance;

public class StartStack implements Executable {
    @Override
    public void execute() throws Exception{
        new CreateInstance().run("Stack");
    }
}