package com.company.homeWorkTen.reflectionInfo.stack;

import com.company.homeWorkTen.reflectionInfo.SequenceBasic;
import com.company.homeWorkTen.reflectionInfo.annotations.TestAnno;
import com.company.homeWorkTen.reflectionInfo.interfaces.ISomeOtherInfo;

import java.util.Random;


@TestAnno(name = "MyTestAnnotation",value = 99)
public class StackWithPriority extends SequenceBasic implements ISomeOtherInfo {
    public String someString = "I'll be back";
    public final Integer someValue = new Random().nextInt(10);
    private Integer secretValue = new Random().nextInt(10);
    private boolean isIt =true;

    public StackWithPriority(){

    }

    public StackWithPriority(String someString){
        this.someString = someString;
    }

    public StackWithPriority(Integer secretValue, boolean isIt){
        this.secretValue = secretValue;
        this.isIt = isIt;
    }


    //Method show the value which located on the top of the StackWithPriority.
    public Integer check(){

        for (int i = 0; i < array[0].length; i++) {
            if(array[0][i] != null)return array[0][i];
        }
        return null;
    }

    //Method allows to take Integer value from StackWithPriority.
    //When Integer take out from StackWithPriority it will be deleted.
    public Integer pop(){
        Integer value = null;

        //Find which element will be the first in Stack and Pop it out from Stack
        //If element not equal to Null then return it from Method and
        //assign Null to both Values and Priorities arrays element.
        for (int i = 0; i < array[0].length; i++) {
            if(array[0][i] != null){
                value = array[0][i];
                array[0][i] = null;
                array[1][i] = null;
                break;
            }
        }

        //If Array is too big but empty it will be resized into twice smaller
        if(array[0].length > 100 && array[0][array[0].length / 2] == null){
            array = resizeArrayDown();
        }

        return value;
    }

    //Method which run after new Element added to Arrays with Values and Priorities
    //It sorts both Arrays according to Priorities. 10 highest Priority 0 the lowest.
    //On the Top should be Values with Highest priority.
    //Method override cause of Stack and Queue has Top from different side.
    @Override
    public void prioritizeSorting(){

        for (int i = 0; i < array[1].length; i++) {
            for (int j = array[1].length - 2; j >= 0; j --) {
                if(array[1][j] != null && array[1][j] < array[1][j + 1]){

                    //Sort values according to priorities
                    int tempValue = array[0][j];
                    array[0][j] = array[0][j + 1];
                    array[0][j + 1] = tempValue;

                    //Sort Priorities from highest to lowest
                    int tempPriority = array[1][j];
                    array[1][j] = array[1][j + 1];
                    array[1][j + 1] = tempPriority;
                }
            }
        }
    }
}
