package com.company.homeWorkTen.reflectionInfo.reflaction;

import com.company.homeWorkTen.reflectionInfo.SequenceBasic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;


public class InfoGatherer {
    private SequenceBasic sequenceInstance;


    public InfoGatherer(SequenceBasic sequenceInstance){
        this.sequenceInstance = sequenceInstance;
        checkInfo();
    }

    private void checkInfo(){
        Integer choice;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("1. getClass() result:");
        System.out.println("2. getClass().getName() result:");
        System.out.println("3. getClass().getModifiers() result:");
        System.out.println("4. getClass().getInterfaces() result:");
        System.out.println("5. getClass().getSuperclass() result:");
        System.out.println("6. getClass().getSuperclass().getName() result:");
        System.out.println("7. sequenceInstance.getClass().getFields() result:");
        System.out.println("8. sequenceInstance.getClass().getDeclaredFields() result:");
        System.out.println("9. sequenceInstance.getClass().getConstructors() result:");
        System.out.println("10. sequenceInstance.getClass().getDeclaredMethods() result:");
        System.out.println("11. sequenceInstance.getClass().getAnnotations() result:");

        try{
            do{
                choice = Integer.parseInt(reader.readLine());
                switch (choice){
                    case 1:
                        aboutClass();
                        break;

                    case 2:
                        nameOfClass();
                        break;
                    case 3:
                        modifiers();
                        break;
                    case 4:
                        interfaces();
                        break;
                    case 5:
                        superClass();
                        break;
                    case 6:
                        superInterfaces();
                        break;

                    case 7:
                        classFields();
                        break;

                    case 8:
                        classDeclaredFields();
                        break;

                    case 9:
                        classConstructors();
                        break;

                    case 10:
                        classMethods();
                        break;

                    case 11:
                        classAnnotations();
                        break;
                }
            }while (choice > 0 && choice < 20);
        }catch (Exception e){
            System.out.println("EXIT");
            System.out.println("====================================================================");
        }
    }

    private void aboutClass(){
        System.out.println(sequenceInstance.getClass());
    }

    private void nameOfClass(){
        System.out.println(sequenceInstance.getClass().getName());
    }

    private void modifiers(){
        System.out.println(Modifier.toString(sequenceInstance.getClass().getModifiers()));
    }

    private void interfaces(){
        Class[] interfaces = sequenceInstance.getClass().getInterfaces();
        for (Class classInterface: interfaces) {
            System.out.println("Interface implements in the object class:  " + classInterface.getName());
        }
    }

    private void superClass(){
        System.out.println("SuperClass:  " + sequenceInstance.getClass().getSuperclass());
    }

    private void superInterfaces(){
        Class[] interfaces = sequenceInstance.getClass().getSuperclass().getInterfaces();
        for (Class superInterface: interfaces) {
            System.out.println("SuperClass Interfaces:  " + superInterface.getName());
        }
    }

    private void classFields(){
        Field[] fields = sequenceInstance.getClass().getFields();
        for (Field field: fields) {
            System.out.println("Fields Name: " + field.getName());
            System.out.println("Field Type: " + field.getType().getName());
        }
    }

    private void classDeclaredFields() {
        Field[] declaredFields = sequenceInstance.getClass().getDeclaredFields();
        for (Field field: declaredFields) {
            System.out.println("Declared Field Name: " + field.getName());
            System.out.println("Declared Field Type: " + field.getType().getName());

            if(Modifier.isPrivate(field.getModifiers()))System.out.println("Declared Field Modification : Private");
            if(Modifier.isPublic(field.getModifiers()))System.out.println("Declared Field Modification : Public");
            if(Modifier.isAbstract(field.getModifiers()))System.out.println("Declared Field Modification : Abstract");
            if(Modifier.isFinal(field.getModifiers()))System.out.println("Declared Field Modification : Final");
        }
    }

    private void classConstructors(){
        Constructor[] classConstructors = sequenceInstance.getClass().getConstructors();
        for (Constructor constructor: classConstructors) {
            System.out.println("Constructor parameters: " + Arrays.toString(constructor.getParameters()));
        }
    }

    private void classMethods(){
        Method[] methods = sequenceInstance.getClass().getDeclaredMethods();
        for ( Method method: methods ) {
            System.out.println("Class Method Name: " + method.getName());
            System.out.println("Class Method Name: " + Arrays.toString(method.getParameterTypes()));
        }
    }

    private void classAnnotations(){
        Annotation[] annotations = sequenceInstance.getClass().getAnnotations();
        for (Annotation annotation: annotations ) {
            System.out.println("Annotation Name: " + annotation.annotationType().getName());
            System.out.println("Annotation Name: " + Arrays.toString(annotation.annotationType().getTypeParameters()));
            System.out.println("Annotation Name: " + annotation.annotationType());
        }
    }
}
