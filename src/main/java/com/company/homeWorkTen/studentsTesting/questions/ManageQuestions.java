package com.company.homeWorkTen.studentsTesting.questions;

import com.company.homeWorkTen.studentsTesting.questions.entities.Questions;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class ManageQuestions {
    private static SessionFactory factory;
    public static void manage(){
        try {
            factory = new Configuration().
                    configure().
                    addAnnotatedClass(Questions.class).
                    buildSessionFactory();

        }catch (Throwable e){
            System.err.println("Failed to create SessionFactory object." + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public Integer addQuestion(String question){
        Session session = factory.openSession();

        Transaction tx = null;
        Integer questionID = null;

        try{
            tx = session.beginTransaction();
            Questions questions = new Questions();
            questions.setQuestion(question);
            questionID = (Integer) session.save(questions);
            tx.commit();
            System.out.println("Question Added to QUESTIONS table.");
        }catch (HibernateException e){
            if(tx != null)tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return questionID;
    }
}
