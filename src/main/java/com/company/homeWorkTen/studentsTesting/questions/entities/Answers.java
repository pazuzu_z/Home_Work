package com.company.homeWorkTen.studentsTesting.questions.entities;


import javax.persistence.*;

@Entity
@Table(name = "ANSWERS")
public class Answers {

    @Column(name = "QUESTION_ID")
    private int questionId;

    @Column(name = "ANSWER")
    private String answer;

    @Column(name = "IS_CORRECT")
    private boolean isCorrect;

    public Answers() {
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
