package com.company.homeWorkTen.studentsTesting;

import com.company.Executable;
import com.company.homeWorkTen.studentsTesting.questions.FillOutQuestions;

import static com.company.homeWorkTen.studentsTesting.questions.ManageQuestions.manage;

public class StartStudentsTesting implements Executable {

    @Override
    public void execute() throws Exception {

        manage();
        new FillOutQuestions().fill();
    }
}
