package com.company.HomeWorkEight.ThreadArrayCalculation;

import com.company.HomeWorkEight.PerformEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class PerformArray extends PerformEntity {
    private int[] intArray; //save an array where sum of elements should be calculated.
    private int step; //depends on number of threads. Show how many elements should be calculated in one thread.
    private int arraySum = 0; //save array sum.
    private ArrayList<ArrayCalculations> arrayCalculations = new ArrayList<>(); //save all calculation results and then helps combine them together.
    private ArrayList<int[]> listOfArrays = new ArrayList<>(); //save a List of arrays on which original array has been split off.

    //Initialize Array where sum should be calculated
    public void createEntity(){

        int size = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Enter size for created Array:  ");
            size = Integer.parseInt(reader.readLine());
        }catch (IOException e){
            System.out.println("IOException:  " + e.getMessage());
        }
        this.intArray = new int[size];
    }

    //Random generates and fill out an array.
    public void fillOutEntity(){
        Random random = new Random();
        for (int i = 0; i < this.intArray.length; i++) {
            this.intArray[i] = random.nextInt(10);
        }
    }

    //print Array with generated values.
    public void getIntEntity(){
        System.out.println(Arrays.toString(this.intArray));
        System.out.println("================================================================");
    }

    //Show length of Array.
    public int getNumberOfElements(){
        return this.intArray.length;
    }

    //Split original Array to a parts to calculate each in it's own thread.
    //Save parts to a listOfArrays List of objects.
    public void splitEntity(){
        //calculate how many elements should be calculated in one iteration(by one thread)
        arrayStep();
        System.out.println("Current array will be calculated via " + this.threads + " threads.");
        for (int i = 0; i < this.threads; i ++) {
            if(i < this.threads - 1){
                this.listOfArrays.add(Arrays.copyOfRange(this.intArray, i * step, i * step + step));

            }else {
                this.listOfArrays.add(Arrays.copyOfRange(this.intArray, i * step, this.intArray.length));
            }
        }
    }

    //Call all methods in correct order to calculate results from splitEntity().
    protected void calcEntity(){
        createCalculationsThreads();
        sumCalculation();
        getArraySum();
    }

    //display sum of Array.
    private void getArraySum(){
        System.out.println("Array sum equal to:  " + this.arraySum);
    }

    //Add new object arrayCalculations with predefined Array.
    private void createCalculationsThreads(){
        for (int[] arrayPart: listOfArrays) {
            arrayCalculations.add( new ArrayCalculations(arrayPart));
        }
    }

    //Calculate sum of Array based on all parts calculated via different threads.
    private void sumCalculation(){
        for (ArrayCalculations calcThread: arrayCalculations) {
            try {
                calcThread.thread.join(); //wait in loop when all threads will finished.
            }catch (InterruptedException e){
                System.out.println("InterruptedException: " + e.getMessage());
            }
            this.arraySum += calcThread.getArraySum();
        }
    }

    //calculate how many elements should be calculated in one iteration(by one thread)
    private void arrayStep(){
        this.step =  Math.round(this.intArray.length / this.threads);
    }

    //Call all methods to clear necessary variables/variable before next iteration.
    protected void clearAfterCalculation(){
        clearCalculationsThreads();
        clearListOfArrays();
        clearArraySum();
    }

    //Clear matrixCalculations object before next iteration.
    private void clearCalculationsThreads(){
        this.arrayCalculations.clear();
    }

    //Clear listOfArrays before next iteration.
    private void clearListOfArrays(){
        this.listOfArrays.clear();
    }
    //Clear arraySum before next iteration.
    private void clearArraySum(){
        this.arraySum = 0;
    }
}
