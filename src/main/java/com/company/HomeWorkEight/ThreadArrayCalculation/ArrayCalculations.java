package com.company.HomeWorkEight.ThreadArrayCalculation;

public class ArrayCalculations implements Runnable{
    public Thread thread; //create object with type of thread.
    private int[] array; //save array[] from constructor.
    private int arraySum = 0; //save sum of a part of the array.

    ArrayCalculations(int[] array){
        this.array = array;
        this.thread = new Thread(this, "ArrayCalculations Thread");
        //System.out.println("Thread created:  " + this.thread);
        this.thread.start(); //start  new Thread.

    }

    //Calculate sum of an array.
    public void run(){

        for (int arrayValue: this.array) {
            this.arraySum += arrayValue;
        }
        //System.out.println("Sum in current Thread:  " + this.arraySum);
    }

    public int getArraySum(){
        return this.arraySum;
    }
}