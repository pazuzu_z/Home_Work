package com.company.HomeWorkEight.ThreadArrayCalculation;
/*
* 1. Реализовать программу которая будет считать сумму массива в нескольких потоках.
Количество потоков (call) и количество элементов массива (N)- вводимые числа.
Элементы массива должны генерироваться.
Реализовать в программе таймер подсчета времени выполнения.
Путем экспериментов определить, оптимальное количество потоков для подсчета суммы.
(Оптимальным считается количество, при котором достигается минимальное время просчета).
Сделайте метод, который автоматически подберет оптимальное количество потоков, для введенного количества N элементов.
* */


import com.company.Executable;
import com.company.HomeWorkEight.CoreRunner;

//Call coreRunner method with correct flag to create instance of CoreRunner for Array.
public class StartArrayThreadCalculation implements Executable {
    @Override
    public void execute() {
        new CoreRunner().coreRunner("Array");
    }
}
