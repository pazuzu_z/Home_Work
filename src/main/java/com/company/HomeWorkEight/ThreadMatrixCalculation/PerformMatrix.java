package com.company.HomeWorkEight.ThreadMatrixCalculation;

import com.company.HomeWorkEight.PerformEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

//Extends  PerformEntity for multiplying matrix.
public class PerformMatrix extends PerformEntity{
    private int step; //depends on number of threads. Show how many elements should be calculated in one thread.
    private ArrayList<MatrixCalculations> matrixCalculations = new ArrayList<>(); //save all calculation results and then helps combine them together.
    private int allLength = 0; //show all elements for the calculated Entity. Helps in threads number calculation.

    private int[][] matrixA; //matrix A.
    private int[][] matrixB; //matrix B.
    private int[][] matrixC; //result matrix of A * B

    private int iBegin = 0; //shows row where should next thread should start calculation.
    private int jBegin = 0; //shows column where should next thread should start calculation.
    private boolean isLast = false; //flag which show is it last thread.

    //Initialize all matrix.
    public void createEntity(){

        int matrixAColumns = 0;
        int matrixBRows = 0;
        int matrixARowsAndMatrixBColumns = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Enter matrix A size of columns:  ");
            matrixAColumns = Integer.parseInt(reader.readLine());

            System.out.print("Enter matrix B size of rows:  ");
            matrixBRows = Integer.parseInt(reader.readLine());

            System.out.print("Enter matrix A size of rows and matrix B size of columns:  ");
            matrixARowsAndMatrixBColumns = Integer.parseInt(reader.readLine());

        }catch (IOException e){
            System.out.println("IOException: " + e.getMessage());
        }
        this.matrixA = new int[matrixARowsAndMatrixBColumns][matrixAColumns];
        this.matrixB = new int[matrixBRows][matrixARowsAndMatrixBColumns];
        this.matrixC = new int[this.matrixB.length][this.matrixA[0].length];
    }

    //Randomly generate and fill out matrix A and B.
    public void fillOutEntity(){

        Random random = new Random();

        for (int i = 0; i < this.matrixA.length; i++) {
            for (int j = 0; j < this.matrixA[0].length; j++) {
                this.matrixA[i][j] = random.nextInt(10);
            }
        }

        for (int i = 0; i < this.matrixB.length; i++) {
            for (int j = 0; j < this.matrixB[0].length; j++) {
                this.matrixB[i][j] = random.nextInt(10);
            }
        }
    }

    //Save number of elements in matrix C.
    public int getNumberOfElements(){
        numberOfAllElements();
        return this.allLength;
    }

    //Print matrix A and B.
    public void getIntEntity(){
        System.out.println("\nMatrix A:");
        System.out.println("================================================================");

        for (int arrayA[]: this.matrixA ) {
            System.out.println(Arrays.toString(arrayA));
        }
        System.out.println("================================================================");

        System.out.println();
        System.out.println("Matrix B:");
        System.out.println("================================================================");

        for (int arrayB[]: this.matrixB ) {
            System.out.println(Arrays.toString(arrayB));
        }
        System.out.println("================================================================");
    }

    //Split matrix C depends on threads variable.
    public void splitEntity(){
        int count = 1; //count of elements which should be calculated on particular iteration.
        int iteration = 1; //count of iterations. shouldn't be more then number of threads.

        //calculate how many elements should be calculated in one iteration(by one thread)
        entityStep();

        //Move through all matrix C and when count of elements equal to value
        // which should be calculated in one iteration call method
        // which will process this sequence and save data to matrixCalculations object.
        for (int i = 0; i < this.matrixC.length; i++) {
            for (int j = 0; j < this.matrixC[0].length; j++) {

                //If iteration equal to number of threads then this should be the last iteration.
                //Then iBegin and jBegin coordinates saved and fag isLast changed to true.
                //And listOfCalcThreads() method call.
                if(iteration == this.threads){
                    this.iBegin = i;
                    this.jBegin = j;
                    this.isLast = true;
                    listOfCalcThreads();
                    break; //Stop cycle cause this condition only true if this is the last iteration.
                //Save coordinates of entry point for the next iteration.
                }else if(count == 1){
                    this.iBegin = i;
                    this.jBegin = j;
                    count ++;
                 //If count equal to the step then  method should be called listOfCalcThreads().
                 //Iteration should be increased.
                 //And count set to 1.
                }else if(count == this.step){
                    iteration ++;
                    count = 1;
                    listOfCalcThreads();
                 //In all other condition just increase count.
                }else count ++;

            }
            //If this is the last iteration then execution of the method should be stopped.
            if(this.isLast)break;
        }
    }

    //Call all methods in correct order to calculate results from splitEntity().
    protected void calcEntity(){
        entityCalculationsInThreads();
    }

    //Add new object MatrixCalculations with predefined variables and objects to the ArrayList matrixCalculations.
    private void listOfCalcThreads(){
        this.matrixCalculations.add( new MatrixCalculations(matrixA, matrixB, iBegin, jBegin, step, isLast));
    }


    //Wait in loop when all calculations will be done in threads and then combine all the results to fill out matrix C.
    private void entityCalculationsInThreads(){

        //Cycle for all elements generates by threads.
        for (MatrixCalculations calcThread: matrixCalculations) {
            try {
                calcThread.thread.join(); //Wait when particular element will finish it calculation.
            }catch (InterruptedException e){
            System.out.println("InterruptedException: " + e.getMessage());
            }
            int countInMatrix = 0; //Count which check is it not the end of matrixCalculations objects.

            //fill out exact row from exact position in matrix C. In case exact matrixCalculations object has Next object.
                for (int j = calcThread.getjBegin(); j < this.matrixC[0].length && countInMatrix < calcThread.getCount(); j++, countInMatrix ++) {
                    this.matrixC[calcThread.getiBegin()][j] = calcThread.getCalculatedList().get(countInMatrix);
            }

            //If matrix C has next row.
            if(calcThread.getiBegin() != this.matrixC.length){
            //Fill out matrix C from exact row and from first column. In case exact matrixCalculations object has Next object.
                for (int i = calcThread.getiBegin() + 1; i < this.matrixC.length && countInMatrix < calcThread.getCount(); i++) {
                    for (int j = 0; j < this.matrixC[0].length && countInMatrix < calcThread.getCount(); j++, countInMatrix ++) {
                        this.matrixC[i][j] = calcThread.getCalculatedList().get(countInMatrix);
                    }
                }
            }
        }
        //Print result matrix C.
        printResultEntity();
    }

    //Print matrix C. Call after all calculations wil be done.
    private void printResultEntity(){
        for (int arrayC[]: this.matrixC ) {
            System.out.println(Arrays.toString(arrayC));
        }
    }

    //Calculate number of elements in matrix C.
    private void numberOfAllElements(){
        this.allLength = this.matrixC.length * this.matrixC[0].length;
    }

    //calculate how many elements should be calculated in one iteration(by one thread)
    private void entityStep(){
        numberOfAllElements();
        this.step = Math.round(allLength / threads);
    }

    //Call all methods to clear necessary variables/variable before next iteration.
    protected void clearAfterCalculation(){
        clearCalculationsThreads();
    }

    //Clear matrixCalculations object before next iteration.
    private void clearCalculationsThreads(){
        this.matrixCalculations.clear();
    }
}
