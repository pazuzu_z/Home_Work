package com.company.HomeWorkEight.ThreadMatrixCalculation;

import java.util.ArrayList;
import java.util.Arrays;

public class MatrixCalculations implements Runnable{
    public Thread thread; //create object with type of thread.
    private int[][] matrixA; //save matrix A taken from constructor.
    private int[][] matrixB; //save matrix B taken from constructor.
    private ArrayList<Integer> calculatedList = new ArrayList<>(); //Save calculated values to build matrix C.
    private int iBegin; //save i coordinate where calculations should start
    private int jBegin; //save j coordinate where calculations should start
    private int count = 0; //save count of elements calculated on this iteration.
    private int step; //save step taken from constructor.
    private boolean isLast; //save isLast taken from constructor.

    MatrixCalculations(int[][] matrixA, int[][] matrixB, int iBegin, int jBegin, int step, boolean isLast){
        this.matrixA = matrixA;
        this.matrixB = matrixB;
        this.iBegin = iBegin;
        this.jBegin = jBegin;
        this.step = step;
        this.isLast = isLast;

        this.thread = new Thread(this, "EntityCalculations Thread");
        //System.out.println("Thread created:  " + this.thread);
        this.thread.start(); //start  new Thread.
    }

    //Start execute when start() method execute.
    //Calculate elements A * B matrix and save them to calculatedList object.
    //All calculations depends on data which was predefined via constructor.
    public void run(){
        int[][] matrixC = new int[matrixB.length][matrixA[0].length]; //analog of matrix C.

        //Logic which will run if it's not last iteration.
        if(!isLast){
            //calculate and save elements to calculatedList object from exact position in matrix C. In case exact matrixCalculations object has Next object.
            for (int j = jBegin; j < matrixC[0].length && count != step; j++, count ++) {
                for (int k = 0; k < matrixA.length; k++) {
                    matrixC[iBegin][j] += matrixB[iBegin][k] * matrixA[k][j];
                }
                calculatedList.add(matrixC[iBegin][j]);
            }

            //calculate and save elements to calculatedList object from first element in the next row in matrix C. In case exact matrixCalculations object has Next object.
            if (iBegin != matrixC.length){
                for (int i = iBegin + 1; i < matrixC.length && count != step; i++) {
                    for (int j = 0; j < matrixC[0].length && count != step; j++, count ++) {
                        for (int k = 0; k < matrixA.length; k++) {

                            matrixC[i][j] += matrixB[i][k] * matrixA[k][j];
                        }
                        calculatedList.add(matrixC[i][j]);
                    }
                }
            }
        //Logic which will run if it's last iteration.
        }else{
            //calculate and save elements to calculatedList object from exact position in matrix C. In case exact matrixCalculations object has Next object.
            for (int j = jBegin; j < matrixC[0].length; j++, count ++) {
                for (int k = 0; k < matrixA.length; k++) {
                    matrixC[iBegin][j] += matrixB[iBegin][k] * matrixA[k][j];
                }
                calculatedList.add(matrixC[iBegin][j]);
            }
            //calculate and save elements to calculatedList object from first element in the next row in matrix C. In case exact matrixCalculations object has Next object.
            if (iBegin != matrixC.length){
                for (int i = iBegin + 1; i < matrixC.length; i++) {
                    for (int j = 0; j < matrixC[0].length; j++, count ++) {
                        for (int k = 0; k < matrixA.length; k++) {

                            matrixC[i][j] += matrixB[i][k] * matrixA[k][j];
                        }
                        calculatedList.add(matrixC[i][j]);
                    }
                }
            }
        }
        //System.out.println(Arrays.toString(calculatedList.toArray()));
    }
    public ArrayList<Integer> getCalculatedList(){
        return calculatedList;
    }

    public int getiBegin() {
        return iBegin;
    }

    public int getjBegin() {
        return jBegin;
    }

    public int getCount() {
        return count;
    }
}
