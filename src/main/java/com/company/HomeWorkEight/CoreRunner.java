package com.company.HomeWorkEight;

import com.company.HomeWorkEight.ThreadArrayCalculation.PerformArray;
import com.company.HomeWorkEight.ThreadMatrixCalculation.PerformMatrix;


//Create correct instance of PerformEntity depends on received flag.
//Run methods for object performEntity in correct order.
public class CoreRunner {
    private PerformEntity performEntity;

    public void coreRunner(String flag){
        if(flag.equals("Matrix")){performEntity = new PerformMatrix();
        }else if(flag.equals("Array")){performEntity = new PerformArray();}

        //Create basis for performEntity object calculations.
        performEntity.createEntity();
        //Generates data and fill out it for performEntity object.
        performEntity.fillOutEntity();
        //Print to Console generated  data.
        performEntity.getIntEntity();
        findBestVariant();
        //Print information which calculation variant was the fastest.
        performEntity.bestResults();
    }

    //Run calculations with different number of threads.
    private void findBestVariant(){
        for (int threads = 1; threads <= performEntity.getNumberOfElements() / 2; threads ++) { //calculate limits of threads
            //Set new number of threads for the next calculation.
            performEntity.setThreads(threads);
            //Divide Entity on threads count before calculations.
            performEntity.splitEntity();
            //calculate and remember process time.
            performEntity.calculateProcessTime();
        }
    }
}