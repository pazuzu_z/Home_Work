package com.company.HomeWorkEight.threadsPool.array.threadsPool;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class WorkStack {
    private final LinkedList<Runnable> stack;
    private final List<PoolWorker> threads = new ArrayList<>();

    public WorkStack(Integer nThreads){
        stack = new LinkedList<>();

        for (int i = 0; i < nThreads; i++) {
            threads.add(new PoolWorker());
            threads.get(i).start();
        }
    }

    public void execute(Runnable r){
        synchronized (stack){
            stack.addFirst(r);
            stack.notify();
        }
    }

    public void stopWorkers(){
        flagToStopWorker();
        notifyWorkersToStop();
        joinAllWorkers();
        showWorkerStates();
    }

    private void flagToStopWorker(){
        for (PoolWorker poolWorker: threads) {
            poolWorker.flagToStop();
        }
    }

    private void notifyWorkersToStop(){
        for (PoolWorker poolWorker: threads) {
            poolWorker.stopWorker();
        }
    }

    private void joinAllWorkers(){
        for (Thread poolWorker: threads) {
            try{
                poolWorker.join();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    private void showWorkerStates(){
        for (Thread poolWorker: threads) {
            System.out.println(poolWorker.getName() + "  " + poolWorker.getState());
        }
    }

    private class PoolWorker extends Thread{
        private boolean flag = true;

        public void run() {
            Runnable r;

            while (flag){
                synchronized (stack){
                    while (stack.isEmpty()){
                        try {
                            stack.wait();
                            if (!flag)return;
                        }catch (InterruptedException e){
                            addNewWorkerToPool();
                            e.printStackTrace();
                        }
                    }
                    r = stack.removeFirst();
                    System.out.println(this.toString());
                }

                try {
                    r.run();
                }catch (RuntimeException e){
                    addNewWorkerToPool();
                    System.out.println("RuntimeException" + e);
                }
            }
        }

        @Override
        public String toString() {
            return this.getName();
        }

        private void stopWorker(){
            synchronized (stack){
                stack.notify();
            }
        }

        private void flagToStop(){
            this.flag = false;
        }

        private void addNewWorkerToPool(){
            threads.add(new PoolWorker());
        }
    }
}

