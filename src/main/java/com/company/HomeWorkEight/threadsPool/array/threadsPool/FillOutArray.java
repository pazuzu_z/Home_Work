package com.company.HomeWorkEight.threadsPool.array.threadsPool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FillOutArray {
    private final int[] randomValuesArray = new int[10];
    private List<RandomValueGenerator> valueGenerators = new ArrayList<>();
    private WorkStack workStack;

    private class RandomValueGenerator extends Thread {
        private final Integer position;
        boolean isUpdated = false;


        RandomValueGenerator(Integer position) {
            this.position = position;
        }
        public void run() {
            for (int i = 0; i < 1_000_000; i++) {
                randomValuesArray[position] = (int) (Math.random() * 10);
            }
            isUpdated = true;
        }
    }

    public void generateRandomValuesArray() {
        createThreadsPool(5);
        generateValues();
        isValuesGenerated();
        printRandomValuesArray();
        stopThreadsPool();
    }

    private void createThreadsPool(int nThreads){
        workStack = new WorkStack(nThreads);
    }

    private void generateValues(){
        for (int i = 0; i < randomValuesArray.length; i++) {
            valueGenerators.add(new RandomValueGenerator(i));
        }
        for (RandomValueGenerator valueGenerator: valueGenerators) {
            workStack.execute(valueGenerator);
        }
    }

    private void isValuesGenerated(){
        for (RandomValueGenerator valueGenerator : valueGenerators) {
            while (!valueGenerator.isUpdated){
                System.out.print("");
            }
        }
    }

    private void printRandomValuesArray(){
        System.out.println(Arrays.toString(randomValuesArray));
    }

    private void stopThreadsPool(){
        workStack.stopWorkers();
    }
}

