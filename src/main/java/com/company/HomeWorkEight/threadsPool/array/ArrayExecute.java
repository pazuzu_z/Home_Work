package com.company.HomeWorkEight.threadsPool.array;

import com.company.Executable;
import com.company.HomeWorkEight.threadsPool.array.threadsPool.FillOutArray;

public class ArrayExecute implements Executable {
    @Override
    public void execute() {
        new FillOutArray().generateRandomValuesArray();
    }
}
