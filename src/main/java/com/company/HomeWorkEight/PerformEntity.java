package com.company.HomeWorkEight;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class PerformEntity {
    protected int threads; //save number of threads.
    private HashMap<Integer, Long> calcTime = new HashMap<>(); //Save how many threads have been used and how much time did it take.

    public abstract void createEntity();
    public abstract void fillOutEntity();
    public abstract void getIntEntity();
    public abstract int getNumberOfElements();
    public abstract void splitEntity();
    protected abstract void calcEntity();
    protected abstract void clearAfterCalculation();


    //Save number of threads.
    public void setThreads(int threads){
        System.out.println("Calculations in " + threads + " threads number.");
        this.threads = threads;
    }

    //Calculate how much time have been spent for calculations via predefined threads number and save this info to calcTime.
    public void calculateProcessTime(){
        long startTime;
        long endTime;
        long spendTime;

        startTime = System.nanoTime();
        calcEntity();
        endTime = System.nanoTime();
        spendTime = endTime - startTime;

        System.out.println("Start Time:  " + startTime + " nanoseconds.");
        System.out.println("End Time:  " + endTime + " nanoseconds.");
        System.out.println("Time Spend:  " + spendTime + " nanoseconds.");
        this.calcTime.put(this.threads, spendTime);
        System.out.println("================================================================");

        clearAfterCalculation();
    }

    //Calculate which result was the best and display this information.
    public void bestResults(){
        long bestTime = this.calcTime.get(1);

        for (int i = 1; i <= this.calcTime.size(); i++) {
            if(this.calcTime.get(i) < bestTime){bestTime = this.calcTime.get(i);}
        }

        for (Map.Entry<Integer, Long> mapEntry: this.calcTime.entrySet()){
            System.out.println("Threads value:  " + mapEntry.getKey() + "   Calculations done in:  " + mapEntry.getValue() + " nanoseconds.");
        }

        for(Iterator<Map.Entry<Integer, Long>> mapEntry = this.calcTime.entrySet().iterator(); mapEntry.hasNext();){
            if(mapEntry.next().getValue() != bestTime){mapEntry.remove();}
        }
        System.out.println("================================================================");
        System.out.println("The fastest calculations done in:  ");
        for (Map.Entry<Integer, Long> mapEntry: this.calcTime.entrySet()){
            System.out.println("Threads value:  " + mapEntry.getKey() + "   Calculations done in:  " + mapEntry.getValue() + " nanoseconds.");
        }
    }
}
