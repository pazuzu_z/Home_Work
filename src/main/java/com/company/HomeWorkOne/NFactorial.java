package com.company.HomeWorkOne;

import com.company.Executable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NFactorial implements Executable{

    public void execute(){

        //Comment next if callRecursive() will be used.
        nFactorialViaFor();

        //Comment next if callRecursivenFactorialViaFor() will be used.
        //callRecursive();
    }

    //Method count N Factorial via While.
    //If Recursive Method needs then comment nFactorialViaFor() method and uncomment callRecursive() and nFactorialViaRecursive()
    public static void nFactorialViaFor(){
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
            System.out.print("Enter N for N-Factorial: ");

            //Read from keyboard N for N-factorial calculation.
            int n = Integer.parseInt(reader.readLine());
            int N = n;
            long nFactorial = 1;

            //Calculate N-Factorial
            if(n != 0){
                while (n != 1){
                    nFactorial *= n;
                    n --;
                }
            }else nFactorial = 0;

            //Show calculation result.
            System.out.println("N-Factorial equal from " + N + ": " + nFactorial);
        }catch (IOException e){
            System.err.println("IOException: " + e.getMessage());
        }
    }

    /*public static void callRecursive(){
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
            System.out.print("Enter N for N-Factorial: ");

            int n = Integer.parseInt(reader.readLine());
            int N = n;
            long nFactorial = 1;

            if (n != 0){
                nFactorial = nFactorialViaRecursive(n);
            }else nFactorial = 0;
            System.out.println("N-Factorial equal from " + N + ": " + nFactorial);

        }catch (IOException e){
            System.err.println("IOException: " + e.getMessage());
        }
    }

    public static long nFactorialViaRecursive(int n){
        if (n == 1)return 1;
    return  nFactorialViaRecursive(n - 1) * n;
    }*/


}
