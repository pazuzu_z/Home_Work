package com.company.HomeWorkOne;

import com.company.Executable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ArraySorts implements Executable{

    public void execute() {
        sortCalls();
    }


    //Method calls all Sorts variants:
    //topCorner(), bottomCorner(), sandClock(), turnSandClock()
    public static void sortCalls(){
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){

            System.out.print("Please enter Arrays Size: ");
            int arraySize = Integer.parseInt(reader.readLine());
            topCorner(arraySize);
            bottomCorner(arraySize);
            sandClock(arraySize);
            turnSandClock(arraySize);

        }catch (IOException e){
            System.err.println("IOException: " + e.getMessage());
        }
    }

    /*
    * Method which generate and display Array as Plus calculate Sum of all numbers and display it:
    * 0 0 0 0 0 0 0
    * 0 0 0 0 0 0
    * 0 0 0 0 0
    * 0 0 0 0
    * 0 0 0
    * 0 0
    * 0
    *
    * */
    public static void topCorner(int arraySize){

        int[][] array = new int[arraySize][arraySize];

        //Generate Array and fill out with values
        System.out.println("Generated Array:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                array[i][j] = (int)(Math.random() * 10);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //Sort and display Array according to description
        int sum = 0;
        System.out.println("Sorted Array as top corner:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length - i; j ++) {
                System.out.print(array[i][j] + " ");
                sum += array[i][j];
            }
            System.out.println();
        }
        System.out.println("Matrix Sum: " + sum);
        System.out.println();
    }

    /*
    * Method which generate and display Array as Plus calculate Sum of all numbers and display it:
    *             0
    *           0 0
    *         0 0 0
    *       0 0 0 0
    *     0 0 0 0 0
    *   0 0 0 0 0 0
    * 0 0 0 0 0 0 0
    * */

    public static void bottomCorner(int arraySize){
        int[][] array = new int[arraySize][arraySize];

        //Generate Array and fill out with values
        System.out.println("Generated Array:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                array[i][j] = (int)(Math.random() * 10);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //Sort and display Array according to description
        int sum = 0;
        System.out.println("Sorted Array as bottom corner:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                if (j > array.length - 2 - i){
                    System.out.print(array[i][j] + " ");
                    sum += array[i][j];
                }else System.out.print("  ");

            }
            System.out.println();
        }
        System.out.println("Matrix Sum: " + sum);
        System.out.println();
    }

    /*
    * Method which generate and display Array as Plus calculate Sum of all numbers and display it:
    * 0 0 0 0 0 0 0
    *   0 0 0 0 0
    *     0 0 0
    *       0
    *     0 0 0
    *   0 0 0 0 0
    * 0 0 0 0 0 0 0
    * */
    public static void sandClock(int arraySize){
        int[][] array = new int[arraySize][arraySize];

        //Generate Array and fill out with values
        System.out.println("Generated Array:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                array[i][j] = (int)(Math.random() * 10);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //Sort and display Array according to description
        int sum = 0;
        System.out.println("Sorted Array as Sand Clock:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                if (j > i - 1 && j < array.length - i) {
                    System.out.print(array[i][j] + " ");
                    sum += array[i][j];
                }else if (j > array.length - 2 - i && j < i + 1){
                    System.out.print(array[i][j] + " ");
                    sum += array[i][j];
                }else System.out.print("  ");

            }
            System.out.println();
        }
        System.out.println("Matrix Sum: " + sum);
        System.out.println();
    }

    /*
    * Method which generate and display Array as Plus calculate Sum of all numbers and display it:
    * 0           0
    * 0 0       0 0
    * 0 0 0   0 0 0
    * 0 0 0 0 0 0 0
    * 0 0 0   0 0 0
    * 0 0       0 0
    * 0           0
    * */
    public static void turnSandClock(int arraySize){
        int[][] array = new int[arraySize][arraySize];

        //Generate Array and fill out with values
        System.out.println("Generated Array:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                array[i][j] = (int)(Math.random() * 10);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //Sort and display Array according to description
        int sum = 0;
        System.out.println("Sorted Array as Turned Sand Clock:");
        for (int i = 0; i < array.length; i ++) {
            for (int j = 0; j < array.length; j ++) {
                if (j < i + 1 && j < array.length - i) {
                    System.out.print(array[i][j] + " ");
                    sum += array[i][j];
                    }else if (j > array.length - 2 - i && j > i - 1){
                    System.out.print(array[i][j] + " ");
                    sum += array[i][j];
                }else System.out.print("  ");

            }
            System.out.println();
        }
        System.out.println("Matrix Sum: " + sum);
        System.out.println();
    }

}
