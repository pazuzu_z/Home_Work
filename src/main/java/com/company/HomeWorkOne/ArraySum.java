package com.company.HomeWorkOne;

import com.company.Executable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ArraySum implements Executable{
    public void execute(){
        arraySum();
    }

    public static void arraySum(){


        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)))
        {
            //Read from keyboard array size.
            System.out.print("Please put Array size: ");
            int arraySize = Integer.parseInt(reader.readLine());

            int[] array = new int[arraySize];
            int arraySum = 0;

            //Generate Array with size entered before and fill out with random values from 0 to 100.
            for (int i = 0; i < array.length; i++) {
                array[i] = (int)((Math.random())  * 100);
            }
            System.out.println(Arrays.toString(array));

            //Calculate Sum of all Array's elements
            for (int i = 0; i < array.length; i++) {
                arraySum += array[i];
            }

            //Display calculated sum
            System.out.println("Array Sum is: " + arraySum);
        }catch (IOException e){
            System.err.println("IOException: " + e.getMessage());
        }

    }
}
