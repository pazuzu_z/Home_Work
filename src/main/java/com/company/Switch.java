package com.company;

import java.util.Scanner;

import com.company.HomeWorkEight.ThreadArrayCalculation.StartArrayThreadCalculation;
import com.company.HomeWorkEight.ThreadMatrixCalculation.StartMatrixThreadCalculation;
import com.company.HomeWorkEight.threadsPool.array.ArrayExecute;
import com.company.HomeWorkFive.Extends.Hospital;
import com.company.HomeWorkFive.FlowerHierarchy.FlowersHierarchy;
import com.company.HomeWorkFive.Mail.CreateMail;
import com.company.HomeWorkFour.BuilderPattern.AdvancedWay.BuilderExecute;
import com.company.HomeWorkFour.BuilderPattern.SimpleWay.SimpleCellPhoneCreator;
import com.company.HomeWorkFour.NotMoreThenThree.RunNotMoreThanThree;
import com.company.HomeWorkFour.Singleton.Run;
import com.company.HomeWorkOne.ArraySorts;
import com.company.HomeWorkOne.ArraySum;
import com.company.HomeWorkOne.NFactorial;
import com.company.HomeWorkSeven.FlowerShopWithExceptionsLog.FlowerShopWithExceptionsExecute;
import com.company.HomeWorkSix.FlowerShop.FlowerShopExecute;
import com.company.HomeWorkTwo.CountSumOfDifferentDimensionArrays;
import com.company.HomeWorkTwo.QueueAndStack.Queue;
import com.company.HomeWorkTwo.QueueAndStack.Stack;
import com.company.homeWorkNine.factoryViaEnum.FactoryRunner;
import com.company.homeWorkNine.singletonViaEnum.EnumSingleton;
import com.company.homeWorkNine.utilsViaEnum.RunUtils;
import com.company.homeWorkTen.reflectionInfo.queue.StartQueue;
import com.company.homeWorkTen.reflectionInfo.stack.StartStack;
import com.company.homeWorkTen.studentsTesting.StartStudentsTesting;

class Switch {
    private static int taskNumber;

    //Method shows Tasks descriptions and Read from keyboard task number which will be execute.
    private static void switchTask() {

        //Tasks description.
        System.out.println("Home Work 1:");
        System.out.println("1. Home Work One: Array Sum.");
        System.out.println("2. N-Factorial count.");
        System.out.println("3. Shows Array Sorts in different ways and Calculate sum of all displayed values.");
        System.out.println("Home work 2:");
        System.out.println("4. SUM of N dimension Array.");
        System.out.println("5. Create Stack and interface to use it.");
        System.out.println("6. Create Queue and interface to use it.");
        System.out.println("Home work 3:");
        System.out.println("7. Create endless Stack with priority and interface to use it.");
        System.out.println("8. Create endless Queue with priority and interface to use it.");
        System.out.println("Home work 4:");
        System.out.println("9. Create Singleton and Standard class and compare their behavior.");
        System.out.println("10. Not more then three class instances could be created.");
        System.out.println("11. Example of Builder Pattern implementation.");
        System.out.println("12. Example of Simple Builder Pattern implementation.");
        System.out.println("Home work 5:");
        System.out.println("13. Flowers hierarchy.");
        System.out.println("14. Extends example.");
        System.out.println("15. Mail Pattern Decorator.");
        System.out.println("Home work 6:");
        System.out.println("16. Flowers Shop implementation.");
        System.out.println("Home work 7:");
        System.out.println("17. Flowers Shop with Exceptions Logs.");
        System.out.println("Home work 8:");
        System.out.println("18. Calculate optimal value of threads for Array's sum calculation.");
        System.out.println("19. Calculate optimal value of threads for Matrix multiplies calculation.");
        System.out.println("Home work 9:");
        System.out.println("20. Singleton via Enum.");
        System.out.println("21. Factory via Enum.");
        System.out.println("22. Utility via Enum.");
        System.out.println("23. Fill In Array via Threadspool.");
        System.out.println("Home work 10:");
        System.out.println("24. Reflection Info. Start Stack.");
        System.out.println("25. Reflection Info. Start Queue.");
        System.out.println("Home work Extra:");
        System.out.println("26. Student Testing.");

        System.out.println();

        //Read from keyboard int Task number.
        Scanner sc = new Scanner(System.in);
        System.out.print("Please Enter Task Number:  ");
        if (sc.hasNext()) {
            taskNumber = sc.nextInt();

            System.out.println();
        }
    }

    //Method run task based on read number in switchTask() method.
    private static Executable inCase() throws Exception {

        if(taskNumber == 1){return new ArraySum();
        }else if (taskNumber == 2){return new NFactorial();
        }else if (taskNumber == 3){return new ArraySorts();
        }else if (taskNumber == 4){return new CountSumOfDifferentDimensionArrays();
        }else if (taskNumber == 5){return new Stack();
        }else if (taskNumber == 6){return new Queue();
        }else if (taskNumber == 7){
        }else if (taskNumber == 8){
        }else if (taskNumber == 9){return new Run();
        }else if (taskNumber == 10){return new RunNotMoreThanThree();
        }else if (taskNumber == 11){return new BuilderExecute();
        }else if (taskNumber == 12){return new SimpleCellPhoneCreator();
        }else if (taskNumber == 13){return new FlowersHierarchy();
        }else if (taskNumber == 14){return new Hospital();
        }else if (taskNumber == 15){return new CreateMail();
        }else if (taskNumber == 16){return new FlowerShopExecute();
        }else if (taskNumber == 17){return new FlowerShopWithExceptionsExecute();
        }else if (taskNumber == 18){return new StartArrayThreadCalculation();
        }else if (taskNumber == 19){return new StartMatrixThreadCalculation();
        }else if (taskNumber == 20){return new EnumSingleton();
        }else if (taskNumber == 21){return new FactoryRunner();
        }else if (taskNumber == 22){return new RunUtils();
        }else if (taskNumber == 23){return new ArrayExecute();
        }else if (taskNumber == 24){return new StartStack();
        }else if (taskNumber == 25){return new StartQueue();
        }else if (taskNumber == 26){return new StartStudentsTesting();
        }throw new RuntimeException("incorrect value");
    }

    //Method run methods switchTask() and inCase()
    static void startExecute() throws Exception {
        switchTask();
        Executable executable = inCase();
        executable.execute();
    }
}
