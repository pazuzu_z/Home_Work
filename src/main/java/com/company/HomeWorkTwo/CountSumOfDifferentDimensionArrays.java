package com.company.HomeWorkTwo;

/*
* 1) Подсчитать сумму N>2 && N<6 мерного массива. Используйте структуру switch-case. Для выбора мерности массива используется меню.
* */

import com.company.Executable;

import java.io.IOException;

public class CountSumOfDifferentDimensionArrays implements Executable{

    public void execute()throws Exception{
        countSumOfDifferentDimensionArrays();
    }
    private static void countSumOfDifferentDimensionArrays() throws IOException {
        char choice;

        //User Interface Description.
        System.out.println("Pick an action:");
        System.out.println("1. Sum count of 3 dimensions Array.");
        System.out.println("2. Sum count of 4 dimensions Array.");
        System.out.println("3. Sum count of 5 dimensions Array.");

        //Implementation of User Interface Control.
        do {
            choice = (char) System.in.read();
        }while(choice < '1' || choice > '3');

        switch (choice){

            case '1':
                countThirdDimension();
                break;


            case '2':
                countFourthDimension();
                break;


            case '3':
                countFifthDimension();
                break;
        }
    }

    //Generate and fill out Array. Count Sum of 3 dimension Array
    public static void countThirdDimension(){
        int SUM = 0;

        //Auto fill out 3 dimension array.
        int arrayThirdDimension[][][] = new int [3][3][3];
        for (int i = 0; i < arrayThirdDimension.length; i++) {
            for (int j = 0; j < arrayThirdDimension.length; j++) {
                for (int k = 0; k < arrayThirdDimension.length; k++) {
                    arrayThirdDimension[i][j][k] = (int)(Math.random() * 10);
                }
            }
        }
        //Sum count of 3 dimension array.
        for (int x [][] : arrayThirdDimension) {
            for (int y [] : x){
                for (int z : y) {
                    SUM += z;
                }
            }
        }
        //Print SUM of 3 dimension array.
        System.out.println("SUM of 3 dimensions Array = " + SUM);
    }

    //Generate and fill out Array. Count Sum of 4 dimension Array
    public static void countFourthDimension(){
        int SUM = 0;

        //Auto fill out 4 dimension array.
        int arrayFourthDimension[][][][] = new int [3][3][3][3];
        for (int i = 0; i < arrayFourthDimension.length; i++) {
            for (int j = 0; j < arrayFourthDimension.length; j++) {
                for (int k = 0; k < arrayFourthDimension.length; k++) {
                    for (int l = 0; l < arrayFourthDimension.length; l++) {
                        arrayFourthDimension[i][j][k][l] = (int)(Math.random() * 10);
                    }
                }
            }
        }
        //Sum count of 4 dimension array.
        for (int x [][][] : arrayFourthDimension) {
            for (int y [][] : x){
                for (int z []: y) {
                    for (int h: z) {
                        SUM += h;
                    }
                }
            }
        }
        //Print SUM of 4 dimension array.
        System.out.println("SUM of 4 dimensions Array = " + SUM);
    }

    //Generate and fill out Array. Count Sum of 5 dimension Array
    public static void countFifthDimension(){
        int SUM = 0;

        //Auto fill out 5 dimension array.
        int arrayFifthDimension[][][][][] = new int [3][3][3][3][3];
        for (int i = 0; i < arrayFifthDimension.length; i++) {
            for (int j = 0; j < arrayFifthDimension.length; j++) {
                for (int k = 0; k < arrayFifthDimension.length; k++) {
                    for (int l = 0; l < arrayFifthDimension.length; l++) {
                        for (int m = 0; m < arrayFifthDimension.length; m++) {
                            arrayFifthDimension[i][j][k][l][m] = (int)(Math.random() * 10);
                        }
                    }
                }
            }
        }
        //Sum count of 5 dimension array.
        for (int x [][][][] : arrayFifthDimension) {
            for (int y [][][] : x){
                for (int z [][]: y) {
                    for (int h[]: z) {
                        for (int g: h) {
                            SUM += g;
                        }
                    }
                }
            }
        }
        //Print SUM of 5 dimension array.
        System.out.println("SUM of 5 dimensions Array = " + SUM);
    }
}

