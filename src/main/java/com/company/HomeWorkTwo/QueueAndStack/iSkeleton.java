package com.company.HomeWorkTwo.QueueAndStack;

public interface iSkeleton {
    void arrayRealization()throws Exception;
    void printAllArrayElements(Integer[] array);
    Integer[] put(int number, Integer array[]);
    Integer pop(Integer[] array);
    Integer check(Integer[] array);
}
