package com.company.HomeWorkTwo.QueueAndStack;

/*
* 3) Создайте класс  для работы с очередью.
* Создайте 2 метода - для внесения в очередь целочисленного элемента и для извлечения элемента из очереди.
* Что такое очередь, можно посмотреть на вики.
* */


import com.company.Executable;

public class Queue extends Skeleton implements Executable{

    public void execute() throws Exception {
        arrayRealization();
    }

    //Method allows to take Integer value from Queue.
    //When Integer take out from Queue it will be deleted.
    //And All Queue will be moved to the Right in to one position.
    public Integer pop(Integer[] array){
        Integer value = null;

        if(array[array.length - 1] != null){
            value = array[array.length - 1];
            shift(array);
        }

        return value;
    }

    //Method show the value which Element will be the Next.
    public Integer check(Integer[] array){
        Integer value = null;
        if(array[array.length - 1] != null) value = array[array.length - 1];
        return value;
    }

    //Method which move Queue when element pop.
    private static Integer[] shift(Integer[] array){
        for (int i = array.length - 1; i >= 1 ; i --) {
            array[i] = array[i - 1];
        }
        return array;
    }
}
