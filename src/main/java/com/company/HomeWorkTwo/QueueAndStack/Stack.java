package com.company.HomeWorkTwo.QueueAndStack;

/*
* 2) Создайте класс для работы со стэком.
* Размер стэка вводится с клавиатуры.
* Посмотрите в книге тему Методы класса, и создайте 3 метода - для внесения целого элемента в стэк,
* для извлечения из стэка, для просмотра значения элемента в вершины стэка. Что такое стэк, можно посмотреть на вики.
* */

import com.company.Executable;

public class Stack extends Skeleton implements Executable{

    public void execute() throws Exception{
        arrayRealization();
    }

    //Method show the value which located on the top of the Stack.
    public Integer check(Integer[] array){
        Integer value = null;
        for (int i = 0; i < array.length; i++) {
            if(array[i] != null)return array[i];
        }
        return value;
    }

    //Method allows to take Integer value from Stack.
    //When Integer take out from Stack it will be deleted.
    public Integer pop(Integer[] array){
        Integer value = null;

        for (int i = 0; i < array.length; i++) {
            if(array[i] != null){
                value = array[i];
                array[i] = null;
                break;
            }
        }
        return value;
    }
}
