package com.company.HomeWorkTwo.QueueAndStack;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public abstract class Skeleton implements iSkeleton{

    //Method which implement the Menu navigation
    public void arrayRealization()throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Integer choice;
        Integer value;

        //Set the Array size
        System.out.print("Enter Array Size: ");
        Integer size = Integer.parseInt(reader.readLine());
        System.out.println();

        Integer array[] = new Integer[size];

        //Described Interface for communication with Array.
        System.out.println("Pick Actions: ");
        System.out.println("1. Add Element to Array.");
        System.out.println("2. Take out Element from Array.");
        System.out.println("3. Check Next element.");
        System.out.println("4. Show all elements in the Array.");
        System.out.println("Any other. Exit.");

        //Implements Interface for working with Queue.
        try {
            do {
                choice = Integer.parseInt(reader.readLine());
                switch (choice){
                    case 1:
                        array = put((int) (Math.random() * 10), array);
                        System.out.println("New value added to Array.");
                        break;

                    case 2:
                        value = pop(array);
                        if(value != null) System.out.println("Value " + value + " took out from Array");
                        else System.out.println("Array is empty.");
                        break;

                    case 3:
                        value = check(array);
                        if(value != null) System.out.println("Next value in Array is " + value);
                        else System.out.println("Array is empty.");
                        break;

                    case 4:
                        printAllArrayElements(array);
                        break;
                }

            }while (choice > 0 && choice < 5);


        }catch (NumberFormatException e){
            System.out.println("Exit");
        }catch (NullPointerException e){
            System.out.println("Exit");
        }
        //Close the Stream.
        reader.close();
    }

    //Method allows to put new generated value to the Array.
    public Integer[] put(int number, Integer array[]){

        //This part recreate Array when it overloaded.
        if(array[0] != null){
            array = resizeArrayUp(array);
        }

        //Add element into Array
        for (int i = array.length - 1; i > - 1; i --) {
            if(array[i] == null){
                array[i] = number;
                break;
            }
        }
        return array;
    }

    //Method allows to take Integer value from Array.
    //When Integer take out from Array it will be deleted.
    //And All Array will be moved to the Right in to one position.
    public abstract Integer pop(Integer[] array);

    //Method show the value which Element will be the Next.
    public abstract Integer check(Integer[] array);

    //Method allows to see visual Array condition and what values it contains
    public void printAllArrayElements(Integer[] array){
        System.out.println("Array contains: " + Arrays.toString(array));
    }

    private Integer[] resizeArrayUp(Integer[] array){

        Integer[] arrayTemp;
        if(array.length == 0 || array.length == 1){
            arrayTemp = new Integer[array.length + 5];
        }else arrayTemp = new Integer[array.length + array.length / 2];


        for (int i = array.length - 1; i >= 0 ; i --) {
            arrayTemp[arrayTemp.length - i - 1] = array[array.length - i - 1];
        }
        System.out.println("Array overloaded and resized.");

        return arrayTemp;
    }
}
