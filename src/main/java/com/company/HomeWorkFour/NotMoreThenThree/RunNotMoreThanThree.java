package com.company.HomeWorkFour.NotMoreThenThree;

import com.company.Executable;

import static com.company.HomeWorkFour.NotMoreThenThree.NotMoreThenThree.getInstances;

public class RunNotMoreThanThree implements Executable{

    public void execute() throws Exception {
        runNotMoreThanThree();
    }

    private static void runNotMoreThanThree(){

        //Method shows and displays a try to create more then limitation of NotMoreThenThree class instances.
        //Catch NullPointerException in case user try to create more then 3 instances of NotMoreThenThree class.
        NotMoreThenThree[] notMoreThenThrees = new NotMoreThenThree[5];

        try{
            for (int i = 0; i < 6; i++) {
                notMoreThenThrees[i] = getInstances();
                System.out.println("The [" + notMoreThenThrees[i].getCount() + "] class instance has been created.");
            }
        }catch (NullPointerException e){
            System.out.println("Instance is not created. Only three class instances could be created.");
        }
    }
}
