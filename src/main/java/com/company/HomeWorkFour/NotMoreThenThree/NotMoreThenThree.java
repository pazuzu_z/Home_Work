package com.company.HomeWorkFour.NotMoreThenThree;

class NotMoreThenThree {

    private static int count;

    //Create simple constructor with instance counter in it.
    //Each time new instance will be created count will be raise.
    private NotMoreThenThree() {
        count++;
    }

    //Method control how many instances could be created.
    //If count of Class instances overload then return NULL instead of new instance.
    static NotMoreThenThree getInstances() {
        NotMoreThenThree instance;

        if (count < 4) {
            instance = new NotMoreThenThree();
            return instance;
        } else return null;
    }

    //Return value of class instance counter.
    int getCount(){
        return count;
    }


}
