package com.company.HomeWorkFour.BuilderPattern.SimpleWay;

class CellPhoneBuilder {

    //Setup default parameters.
    private int price = 100;
    private int memory = 16;
    private double screen = 5.5;

    //Following methods setup CellPhone parameters.
    //Also they return link to them selves.
    //Which allows to setup any number of parameters ar they will be setup by default.
    CellPhoneBuilder buildMemory(int memory) {
        this.memory = memory;
        return this;
    }

    CellPhoneBuilder buildPrice(int price){
        this.price = price;
        return this;
    }

    CellPhoneBuilder buildScreen(double screen){
        this.screen = screen;
        return this;
    }

    //method(constructor) which create CellPhone object with values set up from parameters.
    //If user will use only build() method then default values will be used.
    CellPhone build(){
        CellPhone cellPhone = new CellPhone();
        cellPhone.setPrice(price);
        cellPhone.setMemory(memory);
        cellPhone.setScreen(screen);
        return cellPhone;
    }
}
