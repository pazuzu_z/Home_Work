package com.company.HomeWorkFour.BuilderPattern.SimpleWay;

//Simple class CellPhone with few parameters.
//It uses default constructor.
//It has setters for all parameters and override toString method
public class CellPhone {
    private int price;
    private int memory;
    private double screen;

    void setPrice(int price) {
        this.price = price;
    }

    void setMemory(int memory){
        this.memory = memory;
    }

    void setScreen(double screen){
        this.screen = screen;
    }

    @Override
    public String toString(){
        return "Cell Phone [ Cost = " + price + " , with Memory = " + memory + " , and Screen size = " + screen + " ]";
    }
}
