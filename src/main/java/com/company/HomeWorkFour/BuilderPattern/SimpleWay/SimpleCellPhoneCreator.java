package com.company.HomeWorkFour.BuilderPattern.SimpleWay;

import com.company.Executable;

public class SimpleCellPhoneCreator implements Executable{

    @Override
    public void execute() throws Exception {
        simpleCellPhoneCreator();
    }

    private static void simpleCellPhoneCreator(){

        //Pretty simple method which initialize cellPhone with CellPhoneBuilder.
        //This allows to set any number of cellPhone parameters.
        CellPhone cellPhone = new CellPhoneBuilder()
                                    .buildMemory(64)//If we remove any of parameters then they will be setup by default.
                                    .buildPrice(300)
                                    .buildScreen(7)
                                    .build();

        //Print object of CellPhone class with overrode toString method.
        System.out.println(cellPhone);
    }
}
