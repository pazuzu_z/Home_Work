package com.company.HomeWorkFour.BuilderPattern.AdvancedWay;

//Predefine CellPhone with specific values.
public class IphoneBuilder extends CellPhoneBuilder{

    public void buildPrice() {
        cellPhone.setPrice(500);
    }

    public void buildMemory() {
        cellPhone.setMemory(64);
    }

    public void buildScreen() {
        cellPhone.setScreen(5.5);
    }
}
