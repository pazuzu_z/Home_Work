package com.company.HomeWorkFour.BuilderPattern.AdvancedWay;

import com.company.Executable;

public class BuilderExecute implements Executable{

    @Override
    public void execute() throws Exception {
        builderExecute();
    }

    private static void builderExecute(){
        //Create CellPhoneBuilder with and initialize with exact realization.
        //Then create correct CellPhone depends on what have been sent as argument.
        //After that return correct object via getCellPhone method.
        //Print results via overrode toString method
        Builder builder = new Builder();
        CellPhone cellPhone;
        CellPhoneBuilder cellPhoneBuilder_1 = new SamsungBuilder();
        CellPhoneBuilder cellPhoneBuilder_2 = new IphoneBuilder();

        builder.setCellPhoneBuilder(cellPhoneBuilder_1);
        builder.constructorCellPhone();
        cellPhone = builder.getCellPhone();
        System.out.println(cellPhone);

        builder.setCellPhoneBuilder(cellPhoneBuilder_2);
        builder.constructorCellPhone();
        cellPhone = builder.getCellPhone();
        System.out.println(cellPhone);
    }


}
