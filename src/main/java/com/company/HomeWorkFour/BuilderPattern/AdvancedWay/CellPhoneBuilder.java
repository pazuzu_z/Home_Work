package com.company.HomeWorkFour.BuilderPattern.AdvancedWay;

//Describe structure for the Builder.
abstract class CellPhoneBuilder {
    CellPhone cellPhone;

    CellPhone getCellPhone() {
        return cellPhone;
    }

    void createNewCellPhone(){
        cellPhone = new CellPhone();
    }

    public abstract void buildPrice();
    public abstract void buildMemory();
    public abstract void buildScreen();
}
