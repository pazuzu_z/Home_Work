package com.company.HomeWorkFour.BuilderPattern.AdvancedWay;

class Builder {

    //create via constructorCellPhone() realization for the sent CellPhoneBuilder object.
    private CellPhoneBuilder cellPhoneBuilder;

    void setCellPhoneBuilder(CellPhoneBuilder cellPhoneBuilder){
        this.cellPhoneBuilder = cellPhoneBuilder;
    }

    CellPhone getCellPhone(){
        return cellPhoneBuilder.getCellPhone();
    }

    void constructorCellPhone(){
        cellPhoneBuilder.createNewCellPhone();
        cellPhoneBuilder.buildPrice();
        cellPhoneBuilder.buildMemory();
        cellPhoneBuilder.buildScreen();
    }
}
