package com.company.HomeWorkFour.BuilderPattern.AdvancedWay;

//Predefine CellPhone with specific values.
public class SamsungBuilder extends CellPhoneBuilder {
    public void buildPrice(){
        cellPhone.setPrice(120);
    }

    public void buildMemory(){
        cellPhone.setMemory(32);
    }

    public void buildScreen(){
        cellPhone.setScreen(7);
    }
}
