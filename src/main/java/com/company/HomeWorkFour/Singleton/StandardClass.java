package com.company.HomeWorkFour.Singleton;

//Example of standard class with instance counter in it.
//Created to compare logic of it and Singleton.
class StandardClass {
    private static int count = 1;
    int instanceNumber;

    StandardClass(){
        instanceNumber = count;
        count ++;
    }
}
