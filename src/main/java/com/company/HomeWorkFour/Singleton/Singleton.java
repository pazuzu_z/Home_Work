package com.company.HomeWorkFour.Singleton;

//Example of one of the singleton realization.
class Singleton {
    private static Singleton instance;
    private static int count = 1;
    int instanceNumber;

    //Constructor could be use only by getInstance method.
    //Each time it calls raise instanceNumber variable on 1.
    private Singleton(){
        instanceNumber = count;
        count ++;
    }

    //Method create new instance of Singleton class only in case if instance variable equal to NULL.
    //In all other cases method will return link to previously created instance.
    static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }
}
