package com.company.HomeWorkFour.Singleton;

import com.company.Executable;

import static com.company.HomeWorkFour.Singleton.Singleton.getInstance;

public class Run implements Executable{

    public void execute() throws Exception {
        run();
    }

    private static void run(){

        //Method just show example how Standard class and Singleton implementation differs.
        //Run in cycle creation of few instances for the both classes.
        StandardClass[] standardClasses = new StandardClass[5];

        for (int i = 0; i < 5; i++) {
            standardClasses[i] = new StandardClass();
            System.out.println("The [" + standardClasses[i].instanceNumber + "] Standard class instance has been created.");
        }
        System.out.println("\n=================================================\n");

        Singleton[] singletons = new Singleton[5];

        for (int i = 0; i < 5; i++) {
            singletons[i] = getInstance();
            System.out.println("The [" + singletons[i].instanceNumber + "] Singleton class instance has been created.");
        }

    }
}
