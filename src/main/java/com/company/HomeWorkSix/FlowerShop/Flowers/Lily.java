package com.company.HomeWorkSix.FlowerShop.Flowers;


import com.company.HomeWorkSix.FlowerShop.Flower;

//Class extends Flowers class with it's own realization of bloom() method.
public class Lily extends Flower {

    public Lily(Integer price){
        setCost(price);
        setColor("WHITE");
        setName("LILY");
    }

    @Override
    public void bloom() {
        System.out.println("Lily are Purple and like water.");
    }

}
