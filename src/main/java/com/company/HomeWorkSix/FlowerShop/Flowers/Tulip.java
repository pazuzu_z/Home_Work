package com.company.HomeWorkSix.FlowerShop.Flowers;

import com.company.HomeWorkSix.FlowerShop.Flower;

//Class extends Flowers class with it's own realization of bloom() method.
public class Tulip extends Flower {

    public Tulip(Integer price){
        setCost(price);
        setColor("ROSE");
        setName("TULIP");
    }

    @Override
    public void bloom() {
        System.out.println("Tulip are Rose and bloom in Spring.");
    }
}
