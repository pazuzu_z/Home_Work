package com.company.HomeWorkSix.FlowerShop.Flowers;

import com.company.HomeWorkSix.FlowerShop.Flower;

//Class extends Flowers class with it's own realization of bloom() method.
public class Carnation extends Flower {

    public Carnation(Integer price){
        setCost(price);
        setColor("WHITE");
        setName("CARNATION");
    }

    @Override
    public void bloom() {
        System.out.println("Carnation white and strict.");
    }
}
