package com.company.HomeWorkSix.FlowerShop.Workers.Vendor;

import com.company.HomeWorkSix.FlowerShop.Flower;

import java.util.ArrayList;

public interface Vendor {
    ArrayList<Flower> createBouquet(ArrayList<String> flowersInTheShop);
    void sellBouquet(ArrayList<Flower> bouquet);
    void showBouquet(ArrayList<Flower> flowerArrayList);
    Integer getCountFlowersInTheShop();
    Integer getCash();
}
