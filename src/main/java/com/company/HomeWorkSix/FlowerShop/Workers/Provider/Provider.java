package com.company.HomeWorkSix.FlowerShop.Workers.Provider;

import java.util.HashMap;

public interface Provider {
    HashMap<String, Integer> getProviderPrices();
}
