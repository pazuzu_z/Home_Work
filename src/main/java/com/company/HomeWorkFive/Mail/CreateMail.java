package com.company.HomeWorkFive.Mail;

import com.company.Executable;
import java.io.BufferedReader;
import java.io.InputStreamReader;

//Implements Executable interface this allow to use Factory pattern for the main structure.
//Provide simple choice to user which type of Mail they want to see.
//Return and print Template which user is picking.
public class CreateMail implements Executable {
    @Override
    public void execute() throws Exception {
        run();
    }

    private void run()throws Exception{

        System.out.println("Pick E-mail Example.");
        System.out.println("1. Standard Mail.");
        System.out.println("2. Business Mail.");
        System.out.println();

        System.out.print("Enter value:  ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer choice = Integer.parseInt(reader.readLine());

        MailChoice mailChoice = new MailChoice();
        mailChoice.returnMail(choice).mailWriter();
    }
}
