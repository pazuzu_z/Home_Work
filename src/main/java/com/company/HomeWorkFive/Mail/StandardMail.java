package com.company.HomeWorkFive.Mail;


//Class implements Mail interface and has pretty simple structure.
//Just combine all letter parts into one in mailWriter() method.
public class StandardMail implements Mail {
    private String head;
    private String body;
    private String footer;

    StandardMail(String head, String body, String footer){
        this.head = head;
        this.body = body;
        this.footer = footer;
    }

    @Override
    public void mailWriter() {
        headWriter();
        bodyWriter();
        footerWriter();
    }

    @Override
    public void headWriter() {
        System.out.print(head);
    }

    @Override
    public void bodyWriter() {
        System.out.print(body);
    }

    @Override
    public void footerWriter() {
        System.out.print(footer);
    }
}
