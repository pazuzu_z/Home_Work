package com.company.HomeWorkFive.Mail;

//Class contains the similar text part which all Mail types will use.
//Also contains 3 methods for the create 3 parts of the Letter.
//Plus 3 methods for return separated Mail parts depends on which is needed.
class Text {
    private String head;
    private String body;
    private String footer;

    private void setHead(){
        this.head =   " ________________________________________________________" +
                    "\n|                                                        |" +
                    "\n|  From: BlaBla Blablovicha.                             |";
    }

    String getHead(){
        setHead();
        return this.head;
    }

    private void setBody(){
        this.body = "\n|                                                        |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |" +
                    "\n|  Some letters text...Some letters text...Some letters  |";
    }

    String getBody(){
        setBody();
        return this.body;
    }

    private void setFooter(){
        this.footer = "\n|                                                        |" +
                      "\n|  My sign                                               |";
    }

    String getFooter(){
        setFooter();
        return this.footer;
    }
}
