package com.company.HomeWorkFive.Mail;


//Class implements Mail interface and show how Decorator pattern works.
//It uses object of Mail interface and add some part of realizations into methods from interface Mail.
//Such logic allow to use one class as main object builder and just add some addition parts into it.
public class BusinessMail implements Mail{
    private Mail standardMail;

    BusinessMail(Mail standardMail){
        this.standardMail = standardMail;
    }

    @Override
    public void mailWriter() {
        headWriter();
        bodyWriter();
        footerWriter();
    }

    @Override
    public void headWriter() {
        System.out.println(  " ________________________________________________________" +
                            "\n|                                                        |" +
                            "\n|                                       Hotel Bla Bla    |" +
                            "\n|                                       Some Address     |" +
                            "\n|                                         Some City      |" +
                            "\n|                                                        |" +
                            "\n|  BLA BLA BLA Company                                   |" +
                            "\n|  In Bla City                                           |" +
                            "\n|  And BlaBlaBla Address                                 |" +
                            "\n|                                                        |");
        standardMail.headWriter();

    }

    @Override
    public void bodyWriter() {
        System.out.print("\n|                                                        |" +
                         "\n|  Hell dear Bla Bla                                     |" +
                         "\n|  My name is...                                         |" +
                         "\n|  I want to offer you some Bla bla bla                  |" +
                         "\n|  The main advantages                                   |");
        standardMail.bodyWriter();
        System.out.print("\n|  Thanks in advance.                                    |" +
                         "\n|  If any questions be free to ask                       |");
    }

    @Override
    public void footerWriter() {
        System.out.print("\n|                                                        |" +
                         "\n|  With respect                                          |" );
        standardMail.footerWriter();
        System.out.print("\n|  Phone Number: 5651561616                              |" +
                         "\n|  Skype: bhbjdshf                                       |" +
                         "\n|  Some Department etc.                                  |" +
                         "\n|________________________________________________________|");
    }
}
