package com.company.HomeWorkFive.Mail;


//Return correct Mail interface implementation depends on user choice.
class MailChoice {

    Mail returnMail(Integer choice){
        Text text = new Text();
        if(choice == 1)return new StandardMail(text.getHead(), text.getBody(), text.getFooter());
        else if (choice == 2)return new BusinessMail(new StandardMail(text.getHead(), text.getBody(), text.getFooter()));
        throw new RuntimeException("incorrect value");
    }
}
