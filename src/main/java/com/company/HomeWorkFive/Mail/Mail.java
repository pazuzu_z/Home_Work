package com.company.HomeWorkFive.Mail;

/*
* Eсть класс Mail (письмо) в котором есть 3 характеристики - head, body и footer.
* И метод writeMail -> который печатает письмо, состоящее из этих характеристик.
* Создать механизм, который позволит нам переопределять body письма, например для разных праздников.
* */

//Interface which has 4 methods. 3 which described main Parts for the letter.
//One which combine all parts in one letter.
public interface Mail {
    void headWriter();
    void bodyWriter();
    void footerWriter();
    void mailWriter();
}
