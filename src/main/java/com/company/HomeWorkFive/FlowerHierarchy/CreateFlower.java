package com.company.HomeWorkFive.FlowerHierarchy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//Combine all the logic.
class CreateFlower {

    void run(){
        CreateFlower createFlower = new CreateFlower();
        HashMap<Integer, Flower> flowerMap = createFlower.createFlowerMap(); //create HashMap and call method which fill it put with all Flowers types.
        createFlower.flowersBloom(flowerMap);
        ArrayList<Flower> flowerArrayList = new ArrayList<>();
        createFlower.saveBouquetFlowerList(flowerMap, flowerArrayList);
        createFlower.printBouquet(flowerArrayList);
        System.out.println("Bouquet price is: " + createFlower.bouquetPrice(flowerArrayList));
        System.out.println();
    }

    //Return HashMap with all Flowers types.
    private HashMap<Integer, Flower> createFlowerMap(){
        HashMap<Integer, Flower> flowerMap = new HashMap<>();
        flowerMap.put(1, new Rose().setParameters());
        flowerMap.put(2, new Carnation().setParameters());
        flowerMap.put(3, new Tulip().setParameters());
        flowerMap.put(4, new Lily().setParameters());
        flowerMap.put(5, new Cactus().setParameters());

        return flowerMap;
    }

    //Method call bloom() method for all kind of flowers from the Map.
    //This is an example which show different realizations for the same method in extended classes.
    private void flowersBloom(HashMap<Integer, Flower> flowerMap){
        System.out.println("=======================================================");
        for (Map.Entry<Integer, Flower> flowerSet: flowerMap.entrySet()) {
            Flower value = flowerSet.getValue();
            value.bloom();
        }

        System.out.println("=======================================================");
    }

    //Generates bouquet which could contain different value of Flowers.
    //Random in FOR cycle decide number Flowers in the bouquet.
    //Create ArrayList of Flowers object and store Flowers in it.
    //Calls method getFlowerFromMap(flowerMap) to pick which type of Flowers will be added to the exact bouquet.
    private void saveBouquetFlowerList(HashMap<Integer, Flower> flowerMap, ArrayList<Flower> flowerArrayList){
        for (int i = 0; i < (int)(Math.random() * 10 + 10); i++) {
            flowerArrayList.add(getFlowerFromMap(flowerMap));
        }
    }

    //Return random flower from all Flowers Collection.
    private Flower getFlowerFromMap(HashMap<Integer, Flower> flowerMap){
        return flowerMap.get((int)(Math.random() * 5 + 1));
    }

    //Print Bouquet already filled out with random Flowers.
    private void printBouquet(ArrayList<Flower> flowerArrayList){
        for (Flower flower: flowerArrayList) {
            System.out.println(flower);
        }
    }

    //Calculate exact Bouquet price and return value.
    private Integer bouquetPrice(ArrayList<Flower> flowerArrayList){
        Integer sum = 0;
        for (Flower flower: flowerArrayList) {
            sum += flower.getCost();
        }
        return sum;
    }
}
