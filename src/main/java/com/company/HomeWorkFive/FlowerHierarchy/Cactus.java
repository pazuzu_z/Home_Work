package com.company.HomeWorkFive.FlowerHierarchy;

//Class extends Flowers class with it's own realization of bloom() method.
public class Cactus extends Flower{
    @Override
    public void bloom() {
        System.out.println("Cactus are Green and ugly with spikes");
    }

    //Method allows to set parameters of exact Flowers.
    Flower setParameters(){
        setCost(500);
        setColor("GREEN");
        setName("CACTUS");
        return this;
    }
}
