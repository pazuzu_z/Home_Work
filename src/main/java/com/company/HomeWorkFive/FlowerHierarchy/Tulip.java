package com.company.HomeWorkFive.FlowerHierarchy;

//Class extends Flowers class with it's own realization of bloom() method.
public class Tulip extends Flower {
    @Override
    public void bloom() {
        System.out.println("Tulip are Rose and bloom in Spring.");
    }

    //Method allows to set parameters of exact Flowers.
    Flower setParameters(){
        setCost(20);
        setColor("ROSE");
        setName("TULIP");
        return this;
    }
}
