package com.company.HomeWorkFive.FlowerHierarchy;

/*
* 1) Необходимо реализовать иерархию цветов (для примера, пусть это будут розы, гвоздики, тюльпаны и... что-то на свой вкус).
* Создать несколько объектов-цветов. Собрать букет с определением его стоимости. В букет может входить несколько цветов одного типа.
* */

import com.company.Executable;

//Class extends Executable class with it's own realization of execute() method.
public class FlowersHierarchy implements Executable{

    //Method random will execute run() method  as described in the task.
    @Override
    public void execute() throws Exception {
        for (int i = 0; i < (int) (Math.random() * 2 + 2); i++) {
            CreateFlower createFlower = new CreateFlower();
            createFlower.run();
            System.out.println("=======================================================");
        }
    }
}
