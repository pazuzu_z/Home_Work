package com.company.HomeWorkFive.FlowerHierarchy;

//Class extends Flowers class with it's own realization of bloom() method.
public class Lily extends Flower{
    @Override
    public void bloom() {
        System.out.println("Lily are Purple and like water.");
    }

    //Method allows to set parameters of exact Flowers.
    Flower setParameters(){
        setCost(200);
        setColor("PURPLE");
        setName("LILY");
        return this;
    }
}
