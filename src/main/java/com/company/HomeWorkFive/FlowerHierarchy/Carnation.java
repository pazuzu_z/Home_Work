package com.company.HomeWorkFive.FlowerHierarchy;

//Class extends Flowers class with it's own realization of bloom() method.
public class Carnation extends Flower {
    @Override
    public void bloom() {
        System.out.println("Carnation white and strict.");
    }

    //Method allows to set parameters of exact Flowers.
    Flower setParameters(){
        setCost(80);
        setColor("WHITE");
        setName("CARNATION");
        return this;
    }
}
