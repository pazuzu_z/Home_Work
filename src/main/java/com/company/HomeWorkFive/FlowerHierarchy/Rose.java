package com.company.HomeWorkFive.FlowerHierarchy;

//Class extends Flowers class with it's own realization of bloom() method.
public class Rose extends Flower {

    @Override
    public void bloom() {
        System.out.println("Roses are Red and beautiful with spikes.");
    }

    //Method allows to set parameters of exact Flowers.
    Flower setParameters(){
        setCost(100);
        setColor("RED");
        setName("ROSE");
        return this;
    }
}
