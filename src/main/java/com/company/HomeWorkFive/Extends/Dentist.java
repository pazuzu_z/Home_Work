package com.company.HomeWorkFive.Extends;

//Class extends Doctor class and override heal() method.
//Class override toString() method. Add addition info to toString() implementation from super class.
public class Dentist extends Doctor{
    @Override
    public String heal() {
        return "\nI'm a Dentist. I can heal teeth." + "\n=====================================";
    }

    @Override
    public String toString(){
        return super.toString() + heal();
    }
}
