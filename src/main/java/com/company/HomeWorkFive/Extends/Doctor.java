package com.company.HomeWorkFive.Extends;

//Extends Adult class. Override work() method.
//Has an abstract method heal() cause there could be a different doctors.
//Add addition characteristic  quality and generates it.
//Override toString() to add addition info according new characteristic and behavior.
public abstract class Doctor extends Adult {
    final private Integer quality = (int)(Math.random() * 80 + 20);

    @Override
    public String work(){
        return "\nI'm a Doctor. I can work in Hospital.";
    }

    public abstract String heal();

    @Override
    public String toString(){
        return super.toString() + "\nQuality: " + quality +  work();
    }
}
