package com.company.HomeWorkFive.Extends;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

class CreateDoctorsForHospital {
    //Start point.
    //Create ArrayList of doctors. Initialize it via call getDoctorsList() method.
    //When List will be fill out call method printDoctorsList() for print doctors list.
    void run()throws Exception {

        System.out.println("1. Create a Dentist.");
        System.out.println("2. Create a Surgeon.");
        System.out.println("3. Create a Therapist.");
        System.out.println("Press any other key for exit.");

        ArrayList<Human> hospitalDoctorsList = getDoctorsList();
        printDoctorsList(hospitalDoctorsList);
    }

    //Describe implementation of user interface with a hints.
    //Allows to create a different doctor types with call of the same method.
    private ArrayList<Human> getDoctorsList()throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int choice;
        ArrayList<Human> hospitalDoctorsList = new ArrayList<>();

        try{
            do {
                choice = Integer.parseInt(reader.readLine());
                switch (choice) {
                    case 1:
                        hospitalDoctorsList.add(createDoctor(choice));
                        System.out.println("New Dentist has been added to the Hospital.");
                        break;

                    case 2:
                        hospitalDoctorsList.add(createDoctor(choice));
                        System.out.println("New Surgeon has been added to the Hospital.");
                        break;

                    case 3:
                        hospitalDoctorsList.add(createDoctor(choice));
                        System.out.println("New Therapist has been added to the Hospital.");
                        break;
                }
            } while (choice > 0 && choice < 4);

        }catch (NumberFormatException e){
            System.out.println("Exit");
        }catch (NullPointerException e){
            System.out.println("Exit");
        }

        return hospitalDoctorsList;
    }

    //Depends on argument return correct doctor type instance.
    private Human createDoctor(Integer choice){
        if(choice == 1){return new Dentist();
        }else if(choice ==2){return new Surgeon();
        }else if(choice ==3){return new Therapist();}
        throw new RuntimeException("incorrect value");
    }

    //Print all info about doctors.
    private void printDoctorsList(ArrayList<Human> hospitalDoctorsList){
        for (Human doctor : hospitalDoctorsList) {
            System.out.println(doctor);
        }
    }
}
