package com.company.HomeWorkFive.Extends;

import com.company.Executable;

//Main class.
//implements interface Executable with execute() method. It's allow to use Fabric pattern.
//Include user interface.
public class Hospital implements Executable{
    public void execute() throws Exception {
        CreateDoctorsForHospital createDoctorsForHospital = new CreateDoctorsForHospital();
        createDoctorsForHospital.run();
    }
}
