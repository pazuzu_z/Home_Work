package com.company.HomeWorkFive.Extends;

//Class extends Doctor class and override heal() method.
//Class override toString() method. Add addition info to toString() implementation from super class.
public class Therapist extends Doctor{
    @Override
    public String heal() {
        return "\nI'm a Therapist. I can make a diagnosis." + "\n=====================================";
    }

    @Override
    public String toString(){
        return super.toString() + heal();
    }
}
