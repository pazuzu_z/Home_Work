package com.company.HomeWorkFive.Extends;

//Class extends Human class. It override Human class abstract methods.
//But it has it's own abstract methods.
//Override toString() method in such way that classes which will extends it could get rich to toString() override of the Human class.
public abstract class Adult extends Human{

    //Simple implementation of abstract method of Human class
    @Override
    public void walk(){
        System.out.println("I can easily walk.");
    }

    //Simple implementation of abstract method of Human class
    @Override
    public void talk(){
        System.out.println("I can easily talk.");
    }

    //Abstract method cause there cold be a lot of work implementation.
    public abstract String work();

    //Override toString() method in such way that classes which will extends it could get rich to toString() override of the Human class.
    @Override
    public String toString(){
        return super.toString();
    }
}
