package com.company.HomeWorkFive.Extends;

import java.util.ArrayList;
import java.util.Collections;

//Class which described Humans characteristics and behavior.
//All characteristics generated randomly.
public abstract class Human {
    final private boolean arms = true;
    final private boolean legs = true;
    final private boolean body = true;
    final private boolean head = true;
    final private  Integer age = (int)(Math.random() * 60 + 20);
    final private Integer weight = (int)(Math.random() * 50 + 70);
    final private String sex = setSex();
    final private String name = setName();
    final private String eyesColor = setEyesColor();

    //Method generates Human name according to the gender.
    private String setName(){
        ArrayList<String> maleNameList = new ArrayList<>();
        Collections.addAll(maleNameList, "Igor", "Oleg", "Petr", "Oleksey");
        ArrayList<String> femaleNameList = new ArrayList<>();
        Collections.addAll(femaleNameList, "Katya", "Nastya", "Masha", "Olya");
        return sex.equals("Male") ? maleNameList.get((int) (Math.random() * maleNameList.size())) : femaleNameList.get((int) (Math.random() * femaleNameList.size()));
    }

    //Method generates gender.
    private String setSex(){
        return (int)(Math.random() * 10) > 5 ? "Male" : "Female";
    }

    //Method generates Eyes Color
    private String setEyesColor(){
        ArrayList<String> eyesColorList = new ArrayList<>();
        Collections.addAll(eyesColorList, "Blue", "Green", "Brown", "Black", "Grey");
        return eyesColorList.get((int) (Math.random() * eyesColorList.size()));
    }

    //Methods which described general behavior of Human class which could have different implementation.
    public abstract void walk();
    public abstract void talk();

    //Override toString() method to print Human characteristics.
    @Override
    public String toString(){
        return "Name: " + name + "\nAge: " + age + "\nSex: " + sex + "\nWeight: " + weight + "\nEyes Color: " + eyesColor;
    }

}
